package pl.kcegielko.GLA;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeolocationAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeolocationAppApplication.class, args);
	}

}
