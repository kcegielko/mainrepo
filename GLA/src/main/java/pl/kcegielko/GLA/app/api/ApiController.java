package pl.kcegielko.GLA.app.api;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.kcegielko.GLA.app.models.Geolocation;
import pl.kcegielko.GLA.app.services.GeolocationService;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
@AllArgsConstructor
public class ApiController {

    private final GeolocationService geolocationService;

    @GetMapping(path = "")
    private List<Geolocation> getAll(){
        return geolocationService.getAll();
    }

    @GetMapping(path = "/url", params = {"url"})
    private Geolocation getGeolocationForUrl(@RequestParam("url") String url){
        return geolocationService.getGeolocationForUrl(url);
    }

    @GetMapping(path = "/ip", params = {"ip"})
    private Geolocation getGeolocationForIpAddress(@RequestParam("ip") String ip){
        return geolocationService.getGeolocationForIpAddress(ip);
    }
}
