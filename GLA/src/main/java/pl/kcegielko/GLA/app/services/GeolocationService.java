package pl.kcegielko.GLA.app.services;

import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.kcegielko.GLA.app.dao.GeolocationRepo;
import pl.kcegielko.GLA.app.models.Geolocation;

import java.util.List;

@Service
@AllArgsConstructor
public class GeolocationService {

    private final GeolocationRepo repo;

    public List<Geolocation> getAll(){
        return repo.findAll();
    }

    public Geolocation getGeolocationForUrl(String url){
        return repo.findByUrl(url).orElseThrow(() -> new IllegalStateException("Url not found"));
    }

    public Geolocation getGeolocationForIpAddress(String ip){
        return repo.findByIp(ip).orElseThrow(() -> new IllegalStateException("Ip not found"));
    }

    public Geolocation addGeolocation(Geolocation geolocation){
        repo.save(geolocation);
        return repo.findById(geolocation.getId()).orElseThrow(() -> new IllegalStateException("Geolocation not found"));
    }

    public Geolocation updateGeolocation(Long id, Geolocation geolocation) {
        repo.updateGeolocation(id, geolocation.getLatitude(), geolocation.getLongitude(), geolocation.getIp(), geolocation.getUrl());
        return repo.findById(id).orElseThrow(() -> new IllegalStateException("Geolocation not found"));
    }

    public void removeGeolocation(Long id) {
        repo.deleteById(id);
    }
}
