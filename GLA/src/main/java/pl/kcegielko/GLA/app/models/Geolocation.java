package pl.kcegielko.GLA.app.models;

import lombok.Data;
import lombok.NonNull;

import javax.persistence.*;

@Entity
@Table(name = "geolocations")
@Data
public class Geolocation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    private String longitude;

    @NonNull
    private String latitude;

    @NonNull
    private String ip;

    @NonNull
    private String url;

    public Geolocation(@NonNull String longitude, @NonNull String latitude, @NonNull String ip, @NonNull String url) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.ip = ip;
        this.url = url;
    }

    public Geolocation() {
    }
}
