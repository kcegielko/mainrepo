package pl.kcegielko.GLA.security.utility;

public enum UserRole {
    USER,
    ADMIN
}
