package pl.kcegielko.GLA.security.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.kcegielko.GLA.security.dao.ConfirmationTokenRepo;
import pl.kcegielko.GLA.security.models.ConfirmationToken;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ConfirmationTokenService {

    private final ConfirmationTokenRepo repo;

    public void addConfirmationToken(ConfirmationToken token){
        repo.save(token);
    }

    public Optional<ConfirmationToken> getToken(String token){
        return repo.findByToken(token);
    }

    public int setConfirmedAt(String token) {
        return repo.updateConfirmedAt(
                token, LocalDateTime.now());
    }

    public void removeByUserId(Long id) {
        repo.removeByUserId(id);
    }
}
