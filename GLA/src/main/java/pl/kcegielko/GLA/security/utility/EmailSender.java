package pl.kcegielko.GLA.security.utility;

public interface EmailSender {

    void sendEmail(String destination, String email);

}
