package pl.kcegielko.GLA.security.services;

import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.kcegielko.GLA.security.models.ConfirmationToken;
import pl.kcegielko.GLA.security.utility.PasswordEncoder;
import pl.kcegielko.GLA.security.models.User;
import pl.kcegielko.GLA.security.dao.UserRepo;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepo repo;
    private final PasswordEncoder passwordEncoder;
    private final ConfirmationTokenService confirmationTokenService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return repo.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    public int enableUser(String username){
        return repo.enableUser(username);
    }

    public List<User> getAllUsers(){

        return repo.findAll();

    }

    public String registerUser(User user){

        boolean userExists = repo.findByUsername(user.getUsername()).isPresent();

        if (userExists){
            User u = repo.findByUsername(user.getUsername()).orElseThrow(() -> new UsernameNotFoundException("User not found"));
            if (user.equals(u)){
                if (!u.getEnabled()){
                    String token = UUID.randomUUID().toString();
                    ConfirmationToken confirmationToken = generateConfirmationToken(u, token);
                    confirmationTokenService.addConfirmationToken(confirmationToken);
                    return token;
                } else {
                    throw new IllegalStateException("User with that username already exists");
                }
            } else {
                throw new IllegalStateException("User with that username already exists");
            }
        }

        userExists = repo.findByEmail(user.getEmail()).isPresent();

        if (userExists){
            User u = repo.findByEmail(user.getEmail()).orElseThrow(() -> new UsernameNotFoundException("User not found"));
            if (user.equals(u)){
                if (!u.getEnabled()){
                    String token = UUID.randomUUID().toString();
                    ConfirmationToken confirmationToken = generateConfirmationToken(u, token);
                    confirmationTokenService.addConfirmationToken(confirmationToken);
                    return token;
                } else {
                    throw new IllegalStateException("User with that username already exists");
                }
            } else{
                throw new IllegalStateException("User with that email already exists");
            }
        }

        String encodedPassword = passwordEncoder.bCryptPasswordEncoder().encode(user.getPassword());

        user.setPassword(encodedPassword);

        repo.save(user);

        String token = UUID.randomUUID().toString();

        ConfirmationToken confirmationToken = generateConfirmationToken(user, token);

        confirmationTokenService.addConfirmationToken(confirmationToken);

        return token;

    }

    public User updateUser(Long id, User user) {
        repo.updateUser(id, user.getName(), user.getEmail(), user.getPassword(), user.getUsername());
        return repo.findById(id).orElseThrow(() -> new IllegalStateException("User not found"));
    }

    public void removeUser(Long id) {
        confirmationTokenService.removeByUserId(id);
        repo.deleteById(id);
    }

    private ConfirmationToken generateConfirmationToken(User user, String token){

        ConfirmationToken confirmationToken = new ConfirmationToken();
        confirmationToken.setToken(token);
        confirmationToken.setCreatedAt(LocalDateTime.now());
        confirmationToken.setExpiresAt(LocalDateTime.now().plusMinutes(15));
        confirmationToken.setUser(user);

        return confirmationToken;
    }
}
