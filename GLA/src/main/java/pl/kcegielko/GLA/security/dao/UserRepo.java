package pl.kcegielko.GLA.security.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.kcegielko.GLA.security.models.User;

import java.util.Optional;

@Repository
@Transactional(readOnly = true)
public interface UserRepo extends JpaRepository<User, Long> {

    public Optional<User> findByUsername(String username);

    public Optional<User> findByEmail(String email);

    @Transactional
    @Modifying
    @Query("UPDATE User u " +
            "SET u.enabled = TRUE WHERE u.username = ?1")
    public int enableUser(String email);

    @Transactional
    @Modifying
    @Query("UPDATE User u " +
            "SET u.name = ?2, " +
            "u.email = ?3, " +
            "u.password = ?4, " +
            "u.username = ?5 " +
            "WHERE u.id = ?1")
    public int updateUser(Long id, String name, String email, String password, String username);

}
