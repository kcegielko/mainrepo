package pl.kcegielko.GLA.utility;

import lombok.AllArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import pl.kcegielko.GLA.security.utility.EmailValidator;

import static org.assertj.core.api.Assertions.assertThat;

class EmailValidatorTest {

    private EmailValidator emailValidator;

    @BeforeEach
    void setUp() {
        emailValidator = new EmailValidator();
    }

    @ParameterizedTest(name = "[{index}] {arguments}")
    @DisplayName(value = "Validate email addresses")
    @CsvSource(value = {
            "example@example.com.pl, TRUE",
            "exampleexample.com.pl, FALSE",
            "example@example., FALSE",
            "example@example, FALSE",
            "@example.com.pl, FALSE",
            "example@.com.pl, FALSE",
            "example@, FALSE",
            "example@., FALSE",
    })
    void validateEmailAddress(String email, boolean expected) {

        // When
        boolean isValid = emailValidator.test(email);

        // Then
        assertThat(isValid).isEqualTo(expected);
    }

}
