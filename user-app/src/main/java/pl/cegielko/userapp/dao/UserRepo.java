package pl.cegielko.userapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.cegielko.userapp.model.User;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {

    public Optional<User> findByUsername(String username);

    public Optional<User> findByUsernameAndEmail(String username, String email);

    public boolean existsByEmail(String email);

    public boolean existsByUsername(String email);

    @Transactional
    @Modifying
    @Query("UPDATE User u SET u.isEnabled = true WHERE u.username = ?1")
    public int enableByUsername(String username);

}
