package pl.cegielko.userapp.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class PasswordConfig {

    @Value("${PASSWORD_ENCODER_STRENGTH}")
    private int PASSWORD_ENCODER_STRENGTH;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder(PASSWORD_ENCODER_STRENGTH);
    }

}
