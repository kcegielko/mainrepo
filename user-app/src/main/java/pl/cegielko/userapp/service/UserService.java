package pl.cegielko.userapp.service;

import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.cegielko.userapp.dao.UserRepo;
import pl.cegielko.userapp.model.User;
import pl.cegielko.userapp.model.UserRegistrationToken;

import java.util.List;

import static pl.cegielko.userapp.model.UserRole.ADMIN;
import static pl.cegielko.userapp.model.UserRole.USER;


@Service
@AllArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepo userRepo;
    private final RegistrationService registrationService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepo.findByUsername(username).orElseThrow(() ->new UsernameNotFoundException("Username has not been found"));
    }

    public User registerUser(UserRegistrationToken userRegistrationToken){
        return registrationService.registerUser(userRegistrationToken, USER);
    }

    public User registerAdmin(UserRegistrationToken userRegistrationToken){
        return registrationService.registerUser(userRegistrationToken, ADMIN);
    }

    public int confirmUser(String token){
        return registrationService.confirmUser(token);
    }

    public List<User> getAllUsers(){
        return userRepo.findAll();
    }

    public void deleteUser(Long id) {
        userRepo.deleteById(id);
    }
}
