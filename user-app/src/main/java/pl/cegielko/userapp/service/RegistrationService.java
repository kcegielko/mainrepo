package pl.cegielko.userapp.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.cegielko.userapp.dao.UserRepo;
import pl.cegielko.userapp.model.Authority;
import pl.cegielko.userapp.model.User;
import pl.cegielko.userapp.model.UserRegistrationToken;
import pl.cegielko.userapp.model.UserRole;
import pl.cegielko.userapp.util.PasswordConfig;

import java.util.AbstractMap;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RegistrationService {

    private final EmailService emailService;
    private final UserRepo userRepo;
    private final PasswordConfig passwordConfig;

    @Value("${CONFIRMATION_TOKEN_EXPIRATION_MINUTES}")
    private int CONFIRMATION_TOKEN_EXPIRATION_MINUTES;

    @Value("${CONFIRMATION_TOKEN_SECRET_KEY}")
    private String CONFIRMATION_TOKEN_SECRET_KEY;

    public User registerUser(UserRegistrationToken userRegistrationToken, UserRole userRole){

        if (!emailService.isEmailValid(userRegistrationToken.getEmail())){
            throw new RuntimeException("Email invalid");
        }

        boolean usernameExists = userRepo.existsByUsername(userRegistrationToken.getUsername());
        boolean emailExists = userRepo.existsByEmail(userRegistrationToken.getEmail());

        // THAT USERNAME OR EMAIL ALREADY EXISTS
        if (usernameExists || emailExists){

            // GET USER WITH THAT USERNAME FROM DATABASE
            User databaseUser = userRepo.findByUsernameAndEmail(userRegistrationToken.getUsername(), userRegistrationToken.getEmail())
                    .orElse(null);

            // USERNAME AND EMAIL MATCH TO USER IN DATABASE
            if (databaseUser != null) {

                // USER IN DATABASE NOT ENABLED
                if (!databaseUser.isEnabled()) {

                    // GENERATE CONFIRMATION TOKEN
                    String token = generateToken(databaseUser);

                    // SEND CONFIRMATION TOKEN
                    emailService.sendConfirmationEmail(databaseUser.getEmail(), token, databaseUser.getUsername());

                    // RETURN USER
                    return databaseUser;

                } else { // USER IN DATABASE ENABLED

                    // USERNAME AND EMAIL EXISTS - THROW EXCEPTION
                    throw new RuntimeException("User with that username or email already exists");
                }

            } else { // USERNAME AND EMAIL DOES NOT MACH TO USER IN DATABASE

                // USERNAME OR EMAIL EXISTS - THROW EXCEPTION
                throw new RuntimeException("User with that username or email already exists");
            }

        } else { // USERNAME AND EMAIL DOES NOT EXISTS

            // GENERATE NEW USER
            User user = new User();
            user.setUsername(userRegistrationToken.getUsername());
            user.setPassword(passwordConfig.bCryptPasswordEncoder().encode(userRegistrationToken.getPassword()));
            user.setEmail(userRegistrationToken.getEmail());
            user.setAccountNonExpired(true);
            user.setCredentialsNonExpired(true);
            user.setAccountNonLocked(true);
            user.setEnabled(false);
            user.setAuthorities(List.of(new Authority("ROLE_" + userRole)));

            // GENERATE CONFIRMATION TOKEN
            String token = generateToken(user);

            // SEND CONFIRMATION TOKEN TO EMAIL
            emailService.sendConfirmationEmail(user.getEmail(), token, user.getUsername());

            // SAVE THAT USER TO DATABASE
            userRepo.save(user);

            // RETURN USER
            return user;

        }
    }

    public int confirmUser(String token){

        Jws<Claims> claimsJws = Jwts.parserBuilder()
                .setSigningKey(Keys.hmacShaKeyFor(CONFIRMATION_TOKEN_SECRET_KEY.getBytes()))
                .build().parseClaimsJws(token);

        Claims body = claimsJws.getBody();

        String username = body.getSubject();

        String email = (String) body.get("email");

        User user = userRepo.findByUsernameAndEmail(username, email).orElse(null);

        // USERNAME AND PASSWORD MACH
        if (user == null){
            throw new RuntimeException("Error confirming email");
        }

        return userRepo.enableByUsername(username);
    }

    private String generateToken(User user){
        List<AbstractMap.SimpleEntry<String, String>> authorities = user.getAuthorities().stream()
                .map(m -> new AbstractMap.SimpleEntry<>("authority", m.getAuthority()))
                .collect(Collectors.toList());

        Date date = new Date();


        String token = Jwts.builder()
                .setSubject(user.getUsername())
                .claim("email", user.getEmail())
                .setIssuedAt(date)
                .setExpiration(DateUtils.addMinutes(date, CONFIRMATION_TOKEN_EXPIRATION_MINUTES))
                .signWith(Keys.hmacShaKeyFor(CONFIRMATION_TOKEN_SECRET_KEY.getBytes()))
                .compact();

        return token;
    }
}
