package pl.cegielko.userapp.api;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.cegielko.userapp.model.User;
import pl.cegielko.userapp.model.UserRegistrationToken;
import pl.cegielko.userapp.service.UserService;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class PublicApi {

    private final UserService userService;

    @PostMapping("/register/user")
    private User registerUser(@RequestBody UserRegistrationToken userRegistrationToken){
        return userService.registerUser(userRegistrationToken);
    }

    @GetMapping(value = "/confirm", params = {"token"})
    private int confirmUser(@RequestParam String token){
        return userService.confirmUser(token);
    }

    @GetMapping("/hello")
    private String hello(){
        return "Hello!";
    }

}
