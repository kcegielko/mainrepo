package pl.cegielko.userapp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.stereotype.Component;
import pl.cegielko.userapp.service.UserService;
import pl.cegielko.userapp.util.PasswordConfig;

@Component
public class SecurityAuthenticationProvider extends DaoAuthenticationProvider {

    @Autowired
    public SecurityAuthenticationProvider(PasswordConfig passwordConfig, UserService userService) {

        this.setPasswordEncoder(passwordConfig.bCryptPasswordEncoder());
        this.setUserDetailsService(userService);
    }

}
