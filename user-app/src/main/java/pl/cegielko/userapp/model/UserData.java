package pl.cegielko.userapp.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "users_data")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "second_name")
    private String secondName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "birth_date")
    private String birthDate;

    @OneToOne(mappedBy = "userData", fetch = FetchType.EAGER)
    private User user;

}
