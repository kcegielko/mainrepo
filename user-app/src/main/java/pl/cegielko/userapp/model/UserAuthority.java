package pl.cegielko.userapp.model;

public enum UserAuthority {
    USER_READ,
    USER_WRITE;
}
