package pl.cegielko.userapp.model;

public enum UserRole {
    DATABASE_ADMIN,
    ADMIN,
    USER;
}
