package pl.cegielko.schedulemaker.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.cegielko.schedulemaker.app.model.Issue;
import pl.cegielko.schedulemaker.app.model.Recurrence;

import java.time.LocalDateTime;
import java.util.Set;

@Repository
public interface IssueRepo extends JpaRepository<Issue, Long> {

    public Set<Issue> findByUserId(Long id);

    @Query("SELECT i FROM #{#entityName} i WHERE i.user.id = ?1 AND ((i.startAt <= ?2 AND i.endAt >= ?3 AND i.recurrence IS NULL) OR i.recurrence <= ?4 )")
    public Set<Issue> findSingleIssueByUserIdAndStartAtIsLessThanEqualAndEndAtIsGreaterThanEqual(Long id, LocalDateTime end, LocalDateTime start, Recurrence recurrence);
}
