package pl.cegielko.schedulemaker.app.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Duration;
import java.util.Set;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@DiscriminatorValue("repetitive_issue")
public class RepetitiveIssue extends Issue{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Transient
    private final String type = "REPETITIVE";

    @Column
    @Enumerated(EnumType.ORDINAL)
    private Recurrence recurrence;

    @Column
    private Duration duration;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "issue_id")
    private Set<Task> tasks;

}
