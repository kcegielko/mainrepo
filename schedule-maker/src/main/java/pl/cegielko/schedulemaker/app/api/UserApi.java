package pl.cegielko.schedulemaker.app.api;

import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import pl.cegielko.schedulemaker.app.model.Issue;
import pl.cegielko.schedulemaker.app.model.Recurrence;
import pl.cegielko.schedulemaker.app.model.RepetitiveIssue;
import pl.cegielko.schedulemaker.app.model.SingleIssue;
import pl.cegielko.schedulemaker.security.model.UserData;
import pl.cegielko.schedulemaker.security.service.UserService;

import java.time.LocalDateTime;
import java.util.Set;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/user")
public class UserApi {

    private final UserService userService;

    @GetMapping(value = "/issue/all", params = {"from", "to", "recurrence"})
    private Set<Issue> getUserIssuesFromTo(@RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime from,
                                           @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime to,
                                           @RequestParam("recurrence") Recurrence recurrence){
        return userService.getUserIssuesFromTo(from, to, recurrence);
    }

    @PostMapping(value = "/issue/add/single")
    private Issue addSingleIssue(@RequestBody SingleIssue issue){
        return userService.addIssue(issue);
    }

    @PostMapping(value = "/issue/add/repetitive")
    private Issue addRepetitiveIssue(@RequestBody RepetitiveIssue issue){
        return userService.addIssue(issue);
    }

    @PutMapping(value = "/issue/update/single", params = "id")
    private Issue updateSingleIssue(@RequestBody SingleIssue issue, @RequestParam Long id){
        return userService.updateIssue(issue, id);
    }

    @PutMapping(value = "/issue/update/repetitive", params = "id")
    private Issue updateRepetitiveIssue(@RequestBody RepetitiveIssue issue, @RequestParam Long id){
        return userService.updateIssue(issue, id);
    }

    @DeleteMapping(value = "issue/delete", params = "id")
    private void deleteIssue(@RequestParam Long id){
        userService.deleteIssue(id);
    }

    @PutMapping(value = "/details")
    private UserData updateUserData(@RequestBody UserData userData){
        return userService.updateUserData(userData);
    }

    @GetMapping("/hello")
    private String hello(){
        return "Hello user!";
    }

}
