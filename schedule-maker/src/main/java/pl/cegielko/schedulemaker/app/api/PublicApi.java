package pl.cegielko.schedulemaker.app.api;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.cegielko.schedulemaker.security.model.User;
import pl.cegielko.schedulemaker.security.model.UserRegistrationToken;
import pl.cegielko.schedulemaker.security.service.UserService;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins = "http://localhost:4200")
public class PublicApi {

    private final UserService userService;

    @GetMapping("/users")
    private List<User> getAllUsers(){
        return userService.getAllUsers();
    }

    @PostMapping("/register/user")
    private User registerUser(@RequestBody UserRegistrationToken userRegistrationToken){
        return userService.registerUser(userRegistrationToken);
    }

    @GetMapping(value = "/confirm", params = {"token"})
    private int confirmUser(@RequestParam String token){
        return userService.confirmUser(token);
    }

    @GetMapping("/hello")
    private String hello(){
        return "Hello!";
    }

}
