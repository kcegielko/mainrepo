package pl.cegielko.schedulemaker.app.api;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.cegielko.schedulemaker.security.model.User;
import pl.cegielko.schedulemaker.security.model.UserRegistrationToken;
import pl.cegielko.schedulemaker.security.service.UserService;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/database-administrator")
public class DatabaseAdministratorApi {

    private final UserService userService;

    @PostMapping("/register/admin")
    private User registerAdmin(@RequestBody UserRegistrationToken userRegistrationToken){
        return userService.registerAdmin(userRegistrationToken);
    }

    @DeleteMapping("/delete/{id}")
    private void deleteUser(@PathVariable("id") Long id){
        userService.deleteUser(id);
    }

    @GetMapping("/users")
    private List<User> getAllUsers(){
        return userService.getAllUsers();
    }

}
