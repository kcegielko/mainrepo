package pl.cegielko.schedulemaker.app.model;

public enum Recurrence {
    DAILY,
    WEEKLY,
    MONTHLY,
    YEARLY;
}
