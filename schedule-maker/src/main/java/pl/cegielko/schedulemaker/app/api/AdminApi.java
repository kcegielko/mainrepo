package pl.cegielko.schedulemaker.app.api;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.cegielko.schedulemaker.security.service.UserService;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/admin")
public class AdminApi {

    private final UserService userService;

    @GetMapping("/hello")
    private String hello(){
        return "Hello admin!";
    }

}
