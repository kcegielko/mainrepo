package pl.cegielko.schedulemaker.security.model;

public enum UserRole {
    DATABASE_ADMIN,
    ADMIN,
    USER;
}
