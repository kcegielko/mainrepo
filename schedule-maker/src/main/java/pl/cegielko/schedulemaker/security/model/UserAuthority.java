package pl.cegielko.schedulemaker.security.model;

public enum UserAuthority {
    USER_READ,
    USER_WRITE;
}
