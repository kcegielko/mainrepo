package pl.cegielko.schedulemaker.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import pl.cegielko.schedulemaker.security.model.UserCredentials;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.AbstractMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class JwtUsernameAndPasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final String JWT_SECRET_KEY;

    private final int JWT_EXPIRATION_DAYS;

    private final AuthenticationManager authenticationManager;

    public JwtUsernameAndPasswordAuthenticationFilter(Environment environment, AuthenticationManager authenticationManager) {
        super(authenticationManager);
        this.authenticationManager = authenticationManager;
        this.JWT_SECRET_KEY = environment.getProperty("JWT_SECRET_KEY");
        this.JWT_EXPIRATION_DAYS = Integer.parseInt(Objects.requireNonNull(environment.getProperty("JWT_EXPIRATION_DAYS")));
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {

        try {
            UserCredentials userCredentials = new ObjectMapper().readValue(request.getInputStream(), UserCredentials.class);
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    userCredentials.getUsername(),
                    userCredentials.getPassword()
            ));
            return authentication;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        List<AbstractMap.SimpleEntry<String, String>> authorities = authResult.getAuthorities().stream()
//                .map(m -> m.getAuthority())
                .map(m -> new AbstractMap.SimpleEntry<>("authority", m.getAuthority()))
                .collect(Collectors.toList());


        String token = Jwts.builder()
                .setSubject(authResult.getName())
                .claim("authorities", authorities)
                .setIssuedAt(Date.valueOf(LocalDate.now()))
                .setExpiration(Date.valueOf(LocalDate.now().plusDays(JWT_EXPIRATION_DAYS)))
                .signWith(Keys.hmacShaKeyFor(JWT_SECRET_KEY.getBytes()))
                .compact();

        response.addHeader("authorization", "Bearer " + token);
    }
}
