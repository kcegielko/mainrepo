package pl.cegielko.schedulemaker.security.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import pl.cegielko.schedulemaker.security.filter.JwtTokenVerifierFilter;
import pl.cegielko.schedulemaker.security.filter.JwtUsernameAndPasswordAuthenticationFilter;

import static pl.cegielko.schedulemaker.security.model.UserRole.*;

@Configuration
@EnableWebSecurity
@AllArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final SecurityAuthenticationProvider securityAuthenticationProvider;
    private final Environment environment;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(securityAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.headers().frameOptions().disable(); // FOR H2 DATABASE
        http
//                .csrf().disable()// FOR POSTMAN
//                .csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
//                .and()
                .authorizeRequests()
                .antMatchers("/login").permitAll()
                .antMatchers("/api/v1/user/**").hasAnyRole(DATABASE_ADMIN.name(), ADMIN.name(), USER.name())
                .antMatchers("/api/v1/admin/**").hasAnyRole(DATABASE_ADMIN.name(), ADMIN.name())
                .antMatchers("/api/v1/database-administrator/**").hasRole(DATABASE_ADMIN.name())
                .antMatchers("/api/v1/**").permitAll()
                .antMatchers("/**").permitAll()// FOR H2
                .anyRequest().authenticated()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilter(new JwtUsernameAndPasswordAuthenticationFilter(environment, authenticationManager()))
                .addFilterAfter(new JwtTokenVerifierFilter(environment), JwtUsernameAndPasswordAuthenticationFilter.class);
    }
}
