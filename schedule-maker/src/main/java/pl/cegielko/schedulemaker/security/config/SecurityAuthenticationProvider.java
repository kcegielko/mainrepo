package pl.cegielko.schedulemaker.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.stereotype.Component;
import pl.cegielko.schedulemaker.security.service.UserService;
import pl.cegielko.schedulemaker.security.util.PasswordConfig;

@Component
public class SecurityAuthenticationProvider extends DaoAuthenticationProvider {

    @Autowired
    public SecurityAuthenticationProvider(PasswordConfig passwordConfig, UserService userService) {

        this.setPasswordEncoder(passwordConfig.bCryptPasswordEncoder());
        this.setUserDetailsService(userService);
    }

}
