package pl.cegielko.schedulemaker.security.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.cegielko.schedulemaker.app.dao.IssueRepo;
import pl.cegielko.schedulemaker.app.model.Issue;
import pl.cegielko.schedulemaker.app.model.Recurrence;
import pl.cegielko.schedulemaker.app.model.RepetitiveIssue;
import pl.cegielko.schedulemaker.security.dao.UserRepo;
import pl.cegielko.schedulemaker.security.model.Authority;
import pl.cegielko.schedulemaker.security.model.User;
import pl.cegielko.schedulemaker.security.model.UserData;
import pl.cegielko.schedulemaker.security.model.UserRegistrationToken;
import pl.cegielko.schedulemaker.security.util.PasswordConfig;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import static pl.cegielko.schedulemaker.security.model.UserRole.*;


@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepo userRepo;
    private final IssueRepo issueRepo;
    private final PasswordConfig passwordConfig;
    private final RegistrationService registrationService;

    @Value("${DATABASE_ADMINISTRATOR_USERNAME}")
    private String DATABASE_ADMINISTRATOR_USERNAME;

    @Value("${DATABASE_ADMINISTRATOR_PASSWORD}")
    private String DATABASE_ADMINISTRATOR_PASSWORD;

    @Value("${DATABASE_ADMINISTRATOR_EMAIL}")
    private String DATABASE_ADMINISTRATOR_EMAIL;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        if (username.equals(DATABASE_ADMINISTRATOR_USERNAME)){

            // GENERATE NEW USER
            User user = new User();
            user.setUsername(DATABASE_ADMINISTRATOR_USERNAME);
            user.setPassword(passwordConfig.bCryptPasswordEncoder().encode(DATABASE_ADMINISTRATOR_PASSWORD));
            user.setEmail(DATABASE_ADMINISTRATOR_EMAIL);
            user.setAccountNonExpired(true);
            user.setCredentialsNonExpired(true);
            user.setAccountNonLocked(true);
            user.setEnabled(true);
            user.setAuthorities(Set.of(new Authority("ROLE_" + DATABASE_ADMIN)));
            user.setIssues(Set.of());

            return user;

        }

        return userRepo.findByUsername(username).orElseThrow(() ->new UsernameNotFoundException("Username has not been found"));
    }

    public User registerUser(UserRegistrationToken userRegistrationToken){
        return registrationService.registerUser(userRegistrationToken, USER);
    }

    public User registerAdmin(UserRegistrationToken userRegistrationToken){
        return registrationService.registerUser(userRegistrationToken, ADMIN);
    }

    public int confirmUser(String token){
        return registrationService.confirmUser(token);
    }

    public List<User> getAllUsers(){
        return userRepo.findAll();
    }

    public void deleteUser(Long id) {
        userRepo.deleteById(id);
    }

    public UserData updateUserData(UserData userData) {
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userRepo.findByUsername(username).orElseThrow(() -> new NullPointerException("User does not exists"));
        user.setUserData(userData);
        userRepo.save(user);
        return userData;
    }

    public Issue addIssue(Issue issue) {
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userRepo.findByUsername(username).orElseThrow(() -> new NullPointerException("User does not exists"));
        issue.setUser(user);
        issueRepo.save(issue);
        return issue;
    }

    public Set<Issue> getUserIssuesFromTo(LocalDateTime from, LocalDateTime to, Recurrence recurrence) {
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userRepo.findByUsername(username).orElseThrow(() -> new NullPointerException("User does not exists"));
        return issueRepo.findSingleIssueByUserIdAndStartAtIsLessThanEqualAndEndAtIsGreaterThanEqual(user.getId(), to, from, recurrence);
    }

    public void deleteIssue(Long id) {
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userRepo.findByUsername(username).orElseThrow(() -> new NullPointerException("User does not exists"));
        if (user.getIssues().stream().anyMatch(issue -> issue.getId().equals(id))){
            issueRepo.deleteById(id);
        }
        throw new NullPointerException("User does not have that issue");
    }

    public Issue updateIssue(Issue issue, Long id) {
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userRepo.findByUsername(username).orElseThrow(() -> new NullPointerException("User does not exists"));
        if (user.getIssues().stream().anyMatch(i -> i.getId().equals(id))){
            Issue downloadedIssue = issueRepo.findById(id).orElseThrow(() -> new NullPointerException("Issue with that id does not exists"));
            downloadedIssue.setDescription(issue.getDescription());
            downloadedIssue.setTitle(issue.getTitle());
            downloadedIssue.setStartAt(issue.getStartAt());
            downloadedIssue.setEndAt(issue.getEndAt());
            if (downloadedIssue.getClass().equals(RepetitiveIssue.class) && issue.getClass().equals(RepetitiveIssue.class)){
                ((RepetitiveIssue) downloadedIssue).setRecurrence(((RepetitiveIssue) issue).getRecurrence());
                ((RepetitiveIssue) downloadedIssue).setDuration(((RepetitiveIssue) issue).getDuration());
            }
            issueRepo.save(downloadedIssue);
            return downloadedIssue;
        }
        throw new NullPointerException("User does not have that issue");
    }
}
