package pl.cegielko.schedulemaker.security.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class UserCredentials {

    private String username;
    private String password;

}
