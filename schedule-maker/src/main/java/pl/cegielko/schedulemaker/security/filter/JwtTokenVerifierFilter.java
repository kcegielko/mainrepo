package pl.cegielko.schedulemaker.security.filter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.apache.logging.log4j.util.Strings;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import pl.cegielko.schedulemaker.security.model.Authority;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class JwtTokenVerifierFilter extends OncePerRequestFilter {

    private final String JWT_SECRET_KEY;

    public JwtTokenVerifierFilter(Environment environment) {
        JWT_SECRET_KEY = environment.getProperty("JWT_SECRET_KEY");
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

        String authorizationHeader = httpServletRequest.getHeader("Authorization");

        if (Strings.isNotBlank(authorizationHeader) && Strings.isNotEmpty(authorizationHeader) && authorizationHeader.startsWith("Bearer ")){

            String token = authorizationHeader.replace("Bearer ", "");

            try {

                Jws<Claims> claimsJws = Jwts.parserBuilder()
                        .setSigningKey(Keys.hmacShaKeyFor(JWT_SECRET_KEY.getBytes()))
                        .build().parseClaimsJws(token);

                Claims body = claimsJws.getBody();

                String username = body.getSubject();

                List<Map<String, String>> authorities = (List<Map<String, String>>) body.get("authorities");

                Set<Authority> grantedAuthorities = authorities.stream()
                        .map(m -> new Authority(m.get("authority")))
                        .collect(Collectors.toSet());

                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                        username,
                        null,
                        grantedAuthorities
                );

                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));

                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

                filterChain.doFilter(httpServletRequest, httpServletResponse);

            } catch (JwtException e){

                throw new IllegalStateException(String.format("Invalid token: %s", token), e);
            }

        } else  {

            filterChain.doFilter(httpServletRequest, httpServletResponse);

        }
    }
}
