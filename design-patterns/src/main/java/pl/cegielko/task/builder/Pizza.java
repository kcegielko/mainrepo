package pl.cegielko.task.builder;

public class Pizza {

    private int size = 25;

    private boolean cheese = false;

    private boolean salami = false;

    private boolean onion = false;

    private boolean corn = false;

    private boolean beckon = false;

    @Override
    public String toString() {
        return "Pizza{" +
                "size=" + size +
                ", cheese=" + cheese +
                ", salami=" + salami +
                ", onion=" + onion +
                ", corn=" + corn +
                ", beckon=" + beckon +
                '}';
    }

    public Pizza() {
    }

    public Pizza(int size, boolean cheese, boolean salami, boolean onion, boolean corn, boolean beckon) {
        this.size = size;
        this.cheese = cheese;
        this.salami = salami;
        this.onion = onion;
        this.corn = corn;
        this.beckon = beckon;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isCheese() {
        return cheese;
    }

    public void setCheese(boolean cheese) {
        this.cheese = cheese;
    }

    public boolean isSalami() {
        return salami;
    }

    public void setSalami(boolean salami) {
        this.salami = salami;
    }

    public boolean isOnion() {
        return onion;
    }

    public void setOnion(boolean onion) {
        this.onion = onion;
    }

    public boolean isCorn() {
        return corn;
    }

    public void setCorn(boolean corn) {
        this.corn = corn;
    }

    public boolean isBeckon() {
        return beckon;
    }

    public void setBeckon(boolean beckon) {
        this.beckon = beckon;
    }
}
