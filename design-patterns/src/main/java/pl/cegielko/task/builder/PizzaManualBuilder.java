package pl.cegielko.task.builder;

public class PizzaManualBuilder implements Builder{

    private PizzaManual pizzaManual;

    public PizzaManualBuilder() {
        this.reset();
    }

    @Override
    public void reset() {
        this.pizzaManual = new PizzaManual();
    }

    @Override
    public void setSize(int size) {
        this.pizzaManual.setSize(size);
    }

    @Override
    public void setCheese(boolean cheese) {
        this.pizzaManual.setCheese(cheese);
    }

    @Override
    public void setSalami(boolean salami) {
        this.pizzaManual.setSalami(salami);
    }

    @Override
    public void setOnion(boolean onion) {
        this.pizzaManual.setOnion(onion);
    }

    @Override
    public void setBeckon(boolean beckon) {
        this.pizzaManual.setBeckon(beckon);
    }

    public PizzaManual getPizzaManual(){
        PizzaManual pizzaManual = this.pizzaManual;
        reset();
        return pizzaManual;
    }
}
