package pl.cegielko.task.builder;

public class PizzaDirector {

    public void constructSalami(Builder builder){
        builder.setSize(25);
        builder.setSalami(true);
        builder.setOnion(true);
    }

    public void constructCheese(Builder builder){
        builder.setSize(25);
        builder.setCheese(true);
    }

    public void constructSalamiBeckonOnion(Builder builder){
        builder.setSize(25);
        builder.setSalami(true);
        builder.setOnion(true);
        builder.setBeckon(true);
    }
}
