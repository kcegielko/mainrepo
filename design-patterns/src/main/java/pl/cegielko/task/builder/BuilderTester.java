package pl.cegielko.task.builder;

public class BuilderTester {

    public BuilderTester() {

        System.out.println("\nBuilder tester:");

        PizzaDirector pizzaDirector = new PizzaDirector();

        PizzaBuilder pizzaBuilder = new PizzaBuilder();

        PizzaManualBuilder pizzaManualBuilder = new PizzaManualBuilder();

        // ---------------------------------------------------

        pizzaDirector.constructSalamiBeckonOnion(pizzaBuilder);
        Pizza salamiBeckonOnion = pizzaBuilder.getPizza();

        pizzaDirector.constructSalami(pizzaBuilder);
        Pizza salami = pizzaBuilder.getPizza();

        pizzaDirector.constructCheese(pizzaBuilder);
        Pizza cheese = pizzaBuilder.getPizza();

        // ---------------------------------------------------

        pizzaDirector.constructSalamiBeckonOnion(pizzaManualBuilder);
        PizzaManual salamiBeckonOnionManual = pizzaManualBuilder.getPizzaManual();

        pizzaDirector.constructSalami(pizzaManualBuilder);
        PizzaManual salamiManual = pizzaManualBuilder.getPizzaManual();

        pizzaDirector.constructCheese(pizzaManualBuilder);
        PizzaManual cheeseManual = pizzaManualBuilder.getPizzaManual();

        // ---------------------------------------------------

        System.out.println(salamiBeckonOnion.toString());
        System.out.println(salami.toString());
        System.out.println(cheese.toString());

        System.out.println(salamiBeckonOnionManual.toString());
        System.out.println(salamiManual.toString());
        System.out.println(cheeseManual.toString());
    }
}
