package pl.cegielko.task.builder;

public interface Builder {

    public void reset();

    public void setSize(int size);

    public void setCheese(boolean cheese);

    public void setSalami(boolean salami);

    public void setOnion(boolean onion);

    public void setBeckon(boolean beckon);

}
