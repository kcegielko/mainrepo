package pl.cegielko.task.builder;

public class PizzaBuilder implements Builder{

    private Pizza pizza;

    public PizzaBuilder() {
        this.reset();
    }

    @Override
    public void reset() {
        this.pizza = new Pizza();
    }

    @Override
    public void setSize(int size) {
        this.pizza.setSize(size);
    }

    @Override
    public void setCheese(boolean cheese) {
        this.pizza.setCheese(cheese);
    }

    @Override
    public void setSalami(boolean salami) {
        this.pizza.setSalami(salami);
    }

    @Override
    public void setOnion(boolean onion) {
        this.pizza.setOnion(onion);
    }

    @Override
    public void setBeckon(boolean beckon) {
        this.pizza.setBeckon(beckon);
    }

    public Pizza getPizza() {
        Pizza pizza = this.pizza;
        reset();
        return pizza;
    }
}
