package pl.cegielko.task.observer;

public interface Subscriber {

    void update();

}
