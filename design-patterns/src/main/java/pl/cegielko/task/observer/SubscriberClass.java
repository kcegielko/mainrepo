package pl.cegielko.task.observer;

public class SubscriberClass implements Subscriber{

    @Override
    public void update() {
        System.out.println("SubscriberClass::update()");
    }
}

