package pl.cegielko.task.observer;

public class ObserverTester {

    public ObserverTester() {

        System.out.println("\nObserver tester:");

        PublisherClass publisher = new PublisherClass();

        SubscriberClass subscriber1 = new SubscriberClass();

        SubscriberClass subscriber2 = new SubscriberClass();

        SubscriberClass subscriber3 = new SubscriberClass();

        publisher.addSubscriber(subscriber1);
        publisher.addSubscriber(subscriber2);
        publisher.addSubscriber(subscriber3);

        publisher.notifySubscribers();

        publisher.removeSubscriber(subscriber3);
        publisher.removeSubscriber(subscriber2);

        publisher.notifySubscribers();

    }
}
