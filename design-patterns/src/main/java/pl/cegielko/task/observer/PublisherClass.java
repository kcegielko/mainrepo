package pl.cegielko.task.observer;

import java.util.ArrayList;
import java.util.List;

public class PublisherClass implements Publisher{

    private final List<Subscriber> subscribers = new ArrayList<>();

    @Override
    public void addSubscriber(Subscriber subscriber) {
        System.out.println("PublisherClass::addSubscriber()");
        subscribers.add(subscriber);
    }

    @Override
    public void removeSubscriber(Subscriber subscriber) {
        System.out.println("PublisherClass::removeSubscriber()");
        subscribers.remove(subscriber);
    }

    @Override
    public void notifySubscribers() {
        System.out.println("PublisherClass::notifySubscribers()");
        subscribers.forEach(Subscriber::update);
    }
}
