package pl.cegielko.task.command;

public class PasteCommand implements Command{
    @Override
    public void execute() {
        System.out.println("PasteCommand::execute()");
    }

    @Override
    public void print() {
        System.out.println("PasteCommand::print()");
    }
}
