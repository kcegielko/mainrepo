package pl.cegielko.task.command;

public class DeleteCommand implements Command{
    @Override
    public void execute() {
        System.out.println("DeleteCommand::execute()");
    }

    @Override
    public void print() {
        System.out.println("DeleteCommand::print()");
    }
}
