package pl.cegielko.task.command;

import java.util.ArrayList;
import java.util.List;

public class CommandHistory {

    private static CommandHistory instance;

    private final List<Command> commands = new ArrayList<>();

    private CommandHistory() {
    }

    private static CommandHistory getInstance(){
        if (instance == null){
            instance = new CommandHistory();
        }
        return instance;
    }

    private void pushCommandI(Command command){
        System.out.println("CommandHistory::pushCommandI()");
        commands.add(command);
    }

    private Command popCommandI(){
        System.out.println("CommandHistory::popCommandI()");
        Command command = commands.get(0);
        commands.remove(command);
        return command;
    }

    private void printCommandHistoryI(){
        System.out.println("CommandHistory::printCommandsI()");
        commands.forEach(Command::print);
    }

    public static void pushCommand(Command command){
        System.out.println("CommandHistory::pushCommand()");
        getInstance().pushCommandI(command);
    }

    public static Command popCommand(){
        System.out.println("CommandHistory::popCommand()");
        return getInstance().popCommandI();
    }

    public static void printCommandHistory(){
        System.out.println("CommandHistory::printCommands()");
        getInstance().printCommandHistoryI();
    }

}
