package pl.cegielko.task.command;

public class CommandTester {

    public CommandTester() {

        System.out.println("\nCommand tester:");

        Command command1 = new CopyCommand();
        Command command2 = new CutCommand();
        Command command3 = new PasteCommand();
        Command command4 = new DeleteCommand();

        Button button1 = new Button(command1);
        Button button2 = new Button(command2);

        Slider slider1 = new Slider(command3);
        Slider slider2 = new Slider(command4);

        button1.execute();
        button2.execute();
        slider1.execute();
        slider2.execute();

        CommandHistory.printCommandHistory();
    }
}
