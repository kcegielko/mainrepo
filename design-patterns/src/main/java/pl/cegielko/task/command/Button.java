package pl.cegielko.task.command;

public class Button implements GuiComponent{

    private final Command command;

    public Button(Command command) {
        this.command = command;
    }

    @Override
    public void execute() {
        CommandHistory.pushCommand(command);
        System.out.println("Button::execute()");
        command.execute();
    }
}
