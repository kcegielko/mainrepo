package pl.cegielko.task.command;

public interface Command {

    void execute();

    void print();

}
