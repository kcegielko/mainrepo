package pl.cegielko.task.command;

public interface GuiComponent {

    void execute();

}
