package pl.cegielko.task.command;

public class CutCommand implements Command{
    @Override
    public void execute() {
        System.out.println("CutCommand::execute()");
    }

    @Override
    public void print() {
        System.out.println("CutCommand::print()");
    }
}
