package pl.cegielko.task.command;

public class Slider implements GuiComponent{

    private final Command command;

    public Slider(Command command) {
        this.command = command;
    }

    @Override
    public void execute() {
        CommandHistory.pushCommand(command);
        System.out.println("Slider::execute()");
        command.execute();
    }
}
