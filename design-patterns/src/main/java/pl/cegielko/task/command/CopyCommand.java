package pl.cegielko.task.command;

public class CopyCommand implements Command{
    @Override
    public void execute() {
        System.out.println("CopyCommand::execute()");
    }

    @Override
    public void print() {
        System.out.println("CopyCommand::print()");
    }
}
