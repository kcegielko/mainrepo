package pl.cegielko.task.factoryMethod;

public class RoadDeliveryLogistics extends DeliveryLogistics{

    @Override
    public MeanOfTransport createTransport() {
        return new Truck();
    }

}
