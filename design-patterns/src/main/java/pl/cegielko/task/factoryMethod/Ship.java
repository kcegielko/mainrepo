package pl.cegielko.task.factoryMethod;

public class Ship implements MeanOfTransport{

    @Override
    public void deliver() {
        System.out.println("Ship delivery");
    }

    @Override
    public void cancelDelivery() {
        System.out.println("Cancel ship delivery");
    }
}
