package pl.cegielko.task.factoryMethod;

public abstract class DeliveryLogistics {

    public abstract MeanOfTransport createTransport();

    public void cancelTransport(MeanOfTransport meanOfTransport){
        meanOfTransport.cancelDelivery();
    }

}
