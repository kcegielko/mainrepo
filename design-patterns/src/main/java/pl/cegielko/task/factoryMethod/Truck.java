package pl.cegielko.task.factoryMethod;

public class Truck implements MeanOfTransport{

    @Override
    public void deliver() {
        System.out.println("Truck delivery");
    }

    @Override
    public void cancelDelivery() {
        System.out.println("Cancel truck delivery");
    }
}
