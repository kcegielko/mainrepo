package pl.cegielko.task.factoryMethod;

public class SeaDeliveryLogistics extends DeliveryLogistics{

    @Override
    public MeanOfTransport createTransport() {
        return new Ship();
    }
}
