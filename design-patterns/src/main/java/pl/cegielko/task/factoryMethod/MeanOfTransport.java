package pl.cegielko.task.factoryMethod;

public interface MeanOfTransport {

    public void deliver();

    public void cancelDelivery();

}
