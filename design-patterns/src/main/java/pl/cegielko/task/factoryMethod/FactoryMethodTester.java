package pl.cegielko.task.factoryMethod;

public class FactoryMethodTester {

    public FactoryMethodTester() {

        System.out.println("\nFactory method tester:");

        System.out.println("SEA:");
        DeliveryLogistics deliveryLogistics = new SeaDeliveryLogistics();

        MeanOfTransport meanOfTransport = deliveryLogistics.createTransport();
        meanOfTransport.deliver();
        deliveryLogistics.cancelTransport(meanOfTransport);

        System.out.println("ROAD:");

        deliveryLogistics = new RoadDeliveryLogistics();

        meanOfTransport = deliveryLogistics.createTransport();
        meanOfTransport.deliver();
        deliveryLogistics.cancelTransport(meanOfTransport);

    }
}