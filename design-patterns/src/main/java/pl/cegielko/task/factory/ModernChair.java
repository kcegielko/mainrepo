package pl.cegielko.task.factory;

public class ModernChair implements Chair{

    @Override
    public void sitOn() {
        System.out.println("Sit on modern chair");
    }

    @Override
    public int numberOfLegs() {
        return 1;
    }
}
