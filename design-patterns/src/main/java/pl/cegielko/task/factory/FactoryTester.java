package pl.cegielko.task.factory;

public class FactoryTester {

    public FactoryTester() {

        System.out.println("\nFactory tester:");

        final String factoryType = "MODERN";

        FurnitureFactory furnitureFactory;

        if (factoryType.equals("CLASSIC")){
            furnitureFactory = new ClassicFactory();
        } else if (factoryType.equals("MODERN")) {
            furnitureFactory = new ModernFactory();
        } else {
            throw new IllegalStateException("Unknown factory type");
        }

        Chair chair = furnitureFactory.createChair();
        Sofa sofa = furnitureFactory.createSofa();
        Table table = furnitureFactory.createTable();

        chair.sitOn();
        sofa.sitOn();
        table.putOnTable();

    }
}
