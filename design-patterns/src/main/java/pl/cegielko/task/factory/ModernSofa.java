package pl.cegielko.task.factory;

public class ModernSofa implements Sofa{
    @Override
    public void sitOn() {
        System.out.println("Sit on modern sofa");
    }

    @Override
    public int width() {
        return 180;
    }
}
