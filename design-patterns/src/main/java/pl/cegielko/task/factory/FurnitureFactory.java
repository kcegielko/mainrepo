package pl.cegielko.task.factory;

public interface FurnitureFactory {

    public Chair createChair();

    public Sofa createSofa();

    public Table createTable();

}
