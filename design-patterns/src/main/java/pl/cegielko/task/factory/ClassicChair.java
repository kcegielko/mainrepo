package pl.cegielko.task.factory;

public class ClassicChair implements Chair{

    @Override
    public void sitOn() {
        System.out.println("Sit on classic chair");
    }

    @Override
    public int numberOfLegs() {
        return 4;
    }
}
