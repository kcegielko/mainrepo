package pl.cegielko.task.factory;

public class ClassicTable implements Table{
    @Override
    public void putOnTable() {
        System.out.println("Put on classic table");
    }

    @Override
    public int height() {
        return 100;
    }
}
