package pl.cegielko.task.factory;

public class ClassicSofa implements Sofa{

    @Override
    public void sitOn() {
        System.out.println("Sit on classic sofa");
    }

    @Override
    public int width() {
        return 200;
    }
}
