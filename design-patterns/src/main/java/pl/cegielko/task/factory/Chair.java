package pl.cegielko.task.factory;

public interface Chair {

    public void sitOn();

    public int numberOfLegs();

}
