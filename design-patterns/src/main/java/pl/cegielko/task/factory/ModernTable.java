package pl.cegielko.task.factory;

public class ModernTable implements Table{
    @Override
    public void putOnTable() {
        System.out.println("Put on modern table");
    }

    @Override
    public int height() {
        return 50;
    }
}
