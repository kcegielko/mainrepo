package pl.cegielko.task.factory;

public interface Table {

    public void putOnTable();

    public int height();

}
