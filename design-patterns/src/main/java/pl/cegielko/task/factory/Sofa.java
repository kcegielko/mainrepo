package pl.cegielko.task.factory;

public interface Sofa {

    public void sitOn();

    public int width();

}
