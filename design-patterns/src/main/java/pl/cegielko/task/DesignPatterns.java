package pl.cegielko.task;

import pl.cegielko.task.observer.ObserverTester;
import pl.cegielko.task.builder.BuilderTester;
import pl.cegielko.task.command.CommandTester;
import pl.cegielko.task.factoryMethod.FactoryMethodTester;
import pl.cegielko.task.singleton.SingletonTester;
import pl.cegielko.task.factory.FactoryTester;

public class DesignPatterns {

	public static void main(String[] args) {

		// DESIGN PATTERNS

		FactoryTester factoryTester = new FactoryTester();

		BuilderTester pizzaTester = new BuilderTester();

		FactoryMethodTester factoryMethodTester = new FactoryMethodTester();

		SingletonTester singletonTester = new SingletonTester();

		// BEHAVIORAL PATTERNS

		ObserverTester observerTester = new ObserverTester();

		CommandTester commandTester = new CommandTester();

	}
}
