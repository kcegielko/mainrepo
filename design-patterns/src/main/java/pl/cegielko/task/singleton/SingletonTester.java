package pl.cegielko.task.singleton;

public class SingletonTester {

    public SingletonTester() {

        System.out.println("\nSingleton tester:");

        Singleton singleton = Singleton.getInstance();

        Singleton anotherSingleton = Singleton.getInstance();

        singleton.setLoggerFilePath("Logger path");

        singleton.log();

        anotherSingleton.log();

    }
}
