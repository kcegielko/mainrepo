package pl.cegielko.task.singleton;

public class Singleton {

    private static Singleton instance;

    private static String loggerPath;

    private Singleton() {
    }

    public static Singleton getInstance() {
        if (instance == null){
            instance = new Singleton();
        }
        System.out.println("Singleton::getInstance()");
        return instance;
    }

    public void log(){
        System.out.println("Singleton::log()");
        System.out.println(loggerPath);
    }

    public static void setLoggerFilePath(String loggerFilePath){
        System.out.println("Singleton::setLoggerFilePath()");
        loggerPath = loggerFilePath;
    }
}
