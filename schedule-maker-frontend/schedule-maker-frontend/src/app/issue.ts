export interface Issue {
    title: string;
    description: string;
}