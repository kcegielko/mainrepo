import { Issue } from "./issue";

export interface User {
    id: number;
    username: string;
    email: string;
    password: string;
    isAccountNonLocked: boolean;
    isEnabled: boolean;
    isAccountNonExpired: boolean;
    isCredentialsNonExpired: boolean;
    issues: Issue[];
}