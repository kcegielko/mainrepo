from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

from Window import Window
from Node import Node

class MainWindow(QMainWindow):

	def __init__(self, parent = None):

		super().__init__(parent)

		self.showMaximized()
		self.setWindowTitle('Algorithm visualizer')

		self.window = Window(self)

		self.setCentralWidget(self.window)

		self.toolBar = QToolBar('Edit')

		# ALGORITHM SPINBOX
		self.algorithmLabel = QLabel()
		self.algorithmLabel.setText('Algorithm:')
		self.toolBar.addWidget(self.algorithmLabel)

		self.algorithmComboBox = QComboBox()
		self.algorithmComboBox.currentIndexChanged.connect(self.setAlgorithm)
		self.algorithmComboBox.addItem('A*')
		self.algorithmComboBox.addItem('Dijkstra')
		self.algorithmComboBox.addItem('Best first search')
		self.algorithmComboBox.addItem('Breadth first search')
		self.algorithmComboBox.addItem('Depth first search')
		self.toolBar.addWidget(self.algorithmComboBox)

		# MAZE SIZE SPINBOX
		self.mazeWidthLabel = QLabel()
		self.mazeWidthLabel.setText('Maze width:')
		self.toolBar.addWidget(self.mazeWidthLabel)

		self.mazeWidthComboBox = QSpinBox()
		self.mazeWidthComboBox.setMaximum(100)
		self.mazeWidthComboBox.setMinimum(3)
		self.toolBar.addWidget(self.mazeWidthComboBox)

		self.mazeHeightLabel = QLabel()
		self.mazeHeightLabel.setText('Maze height:')
		self.toolBar.addWidget(self.mazeHeightLabel)

		self.mazeHeightComboBox = QSpinBox()
		self.mazeHeightComboBox.setMaximum(100)
		self.mazeHeightComboBox.setMinimum(3)
		self.toolBar.addWidget(self.mazeHeightComboBox)

		# BUTTONS
		self.clearButton = QPushButton()
		self.clearButton.pressed.connect(self.clear)
		self.clearButton.setText('Clear')
		self.toolBar.addWidget(self.clearButton)

		self.createMazeButton = QPushButton()
		self.createMazeButton.pressed.connect(self.createMaze)
		self.createMazeButton.setText('Create maze')
		self.toolBar.addWidget(self.createMazeButton)

		self.addStartNodeButton = QPushButton()
		self.addStartNodeButton.pressed.connect(self.addStartNode)
		self.addStartNodeButton.setText('Add start node')
		self.toolBar.addWidget(self.addStartNodeButton)

		self.addEndNodeButton = QPushButton()
		self.addEndNodeButton.pressed.connect(self.addEndNode)
		self.addEndNodeButton.setText('Add end node')
		self.toolBar.addWidget(self.addEndNodeButton)

		self.runButton = QPushButton()
		self.runButton.pressed.connect(self.run)
		self.runButton.setText('Run')
		self.toolBar.addWidget(self.runButton)

		self.stopButton = QPushButton()
		self.stopButton.pressed.connect(self.stop)
		self.stopButton.setText('Stop')
		self.toolBar.addWidget(self.stopButton)

		# LABELS
		self.timeElapsedPrefix = QLabel()
		self.timeElapsedPrefix.setText('Time elapsed: ')
		self.toolBar.addWidget(self.timeElapsedPrefix)

		self.timeElapsedLabel = QLabel()
		self.timeElapsedLabel.setText('0')
		self.toolBar.addWidget(self.timeElapsedLabel)

		self.timeElapsedSuffix = QLabel()
		self.timeElapsedSuffix.setText('s')
		self.toolBar.addWidget(self.timeElapsedSuffix)

		# TOOLBAR
		# self.toolBar.setFixedSize(36, 36)
		self.addToolBar(self.toolBar)

	def clear(self):

		self.window.clearScene()

	def refresh(self):
		pass

	def undo(self):
		pass

	def redo(self):
		pass

	def setAlgorithm(self):
		
		pass

	def addStartNode(self):
		
		self.window.view.addStartNode = True

	def addEndNode(self):
		
		self.window.view.addEndNode = True

	def createMaze(self):

		self.window.createMaze(self.mazeWidthComboBox.value(), self.mazeHeightComboBox.value())

	def run(self):

		algorithm = self.algorithmComboBox.currentText()

		if algorithm == 'A*':
			self.window.aStarAlgorithm()

		elif algorithm == 'Dijkstra':
			self.window.dijkstraAlgorithm()

		elif algorithm == 'Best first search':
			self.window.bestFirstSearchAlgorithm()

		elif algorithm == 'Breadth first search':
			self.window.breadthFirstSearchAlgorithm()

		elif algorithm == 'Depth first search':
			self.window.depthFirstSearchAlgorithm()

	def stop(self):

		self.window.stopAlgorithm()

	def setTimeElapsed(self, time):

		self.timeElapsedLabel.setText(time)