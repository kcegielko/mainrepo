import math
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from math import *
from time import sleep, time
from numpy import *
from mazelib import Maze
from mazelib.generate.BacktrackingGenerator import BacktrackingGenerator

from Line import Line
from Node import Node
from StartNode import StartNode
from EndNode import EndNode
from ObstructionNode import ObstructionNode
from ColorNodes import *
from AStarAlgorithm import AStarAlgorithm

class Scene(QGraphicsScene):

    backgroundColor = QColor('#EFF7F6')
    smallGridColor = QColor('#393939')
    smallGridPen = QPen(smallGridColor)
    sceneWidth = 64000
    sceneHeight = 64000
    gridSize = 10
    view = None

    stopSimulation = False
    startNode = None
    endNode = None
    obstructionNodes = []
    openSet = []
    closedSet = []
    path = []

    def __init__(self, parent = None):

        super().__init__(parent)

        self.smallGridPen.setWidthF(0.1)

        self.setSceneRect(-self.sceneWidth*0, -self.sceneHeight*0, self.sceneWidth, self.sceneHeight)
        self.setBackgroundBrush(self.backgroundColor)


    def drawBackground(self, painter: QPainter, rect: QRectF):

        super().drawBackground(painter, rect)

        left = int(math.floor(rect.left()))
        right = int(math.ceil(rect.right()))
        top = int(math.floor(rect.top()))
        bottom = int(math.ceil(rect.bottom()))
        firstLeftLine = left - (left % self.gridSize)
        firstTopLine = top - (top % self.gridSize)

        smallGridLines = []
        for x in range(firstLeftLine, right, self.gridSize):
            smallGridLines.append(QLine(x, top, x, bottom))

        for y in range(firstTopLine, bottom, self.gridSize):
            smallGridLines.append(QLine(left, y, right, y))

        painter.setPen(self.smallGridPen)
        painter.drawLines(*smallGridLines)

    def addItem(self, item: QGraphicsItem):

        super().addItem(item)

    def removeNode(self, x, y):

        item = self.itemAt(x, y, self.view.viewportTransform())

        if isinstance(item, Node):

            if isinstance(item, StartNode):

                self.removeItem(item)
                self.startNode = None

            elif isinstance(item, EndNode):

                self.removeItem(item)
                self.endNode = None

            elif isinstance(item, ObstructionNode):

                self.removeItem(item)
                self.obstructionNodes.remove(item.position)

    def addStartNode(self, x, y):

        hasStartNode = False

        for item in self.items():

            if isinstance(item, StartNode):

                hasStartNode = True

        if hasStartNode == False:

            nodeX = floor(x / self.gridSize) * self.gridSize
            nodeY = floor(y / self.gridSize) * self.gridSize

            self.startNode = StartNode(self, (nodeX, nodeY))
            self.addItem(self.startNode)

    def addEndNode(self, x, y):

        hasEndNode = False

        for item in self.items():

            if isinstance(item, EndNode):

                hasEndNode = True

        if hasEndNode == False:

            nodeX = floor(x / self.gridSize) * self.gridSize
            nodeY = floor(y / self.gridSize) * self.gridSize

            self.endNode = EndNode(self, (nodeX, nodeY))
            self.addItem(self.endNode)

    def addObstructionNode(self, x, y):

        # TUTAJ NALEŻY SPRAWDZAĆ CZY NIE MA NODA W TYM MIEJSCU
        nodeX = floor(x / self.gridSize) * self.gridSize
        nodeY = floor(y / self.gridSize) * self.gridSize

        node = ObstructionNode(self, (nodeX, nodeY))

        if isinstance(self.startNode, Node):
            
            if node == self.startNode:

                return

        if isinstance(self.endNode, Node):
            
            if node == self.endNode:

                return

        self.obstructionNodes.append((nodeX, nodeY))
        self.addItem(node)

    def removeLines(self):

        items = self.items()

        for item in items:

            if isinstance(item, Line):

                self.removeItem(item)

    def removeDuplicatedNodes(self):

        self.obstructionNodes = list(set(self.obstructionNodes))

        # REMOVE ALL NODES
        for item in self.items():

            if isinstance(item, Node):

                self.removeItem(item)

        # ADD START NODE
        if  isinstance(self.startNode, Node):

            self.addItem(self.startNode)

        # ADD END NODE
        if isinstance(self.endNode, Node):

            self.addItem(self.endNode)

        # ADD OBSTRUCTION NODES
        for node in self.obstructionNodes:

            self.addItem(ObstructionNode(self, (node[0], node[1])))

    def removeAllNodes(self):

        for item in self.items():

          if isinstance(item, Node):

              self.removeItem(item)

        self.obstructionNodes.clear()

        self.startNode = None
        self.endNode = None

    def removeColorNodes(self):

        for item in self.items():

          if isinstance(item, YellowNode) or isinstance(item, BlueNode) or isinstance(item, VioletNode) or isinstance(item, PinkNode):

              self.removeItem(item)

    def createMaze(self, width, height):

        m = Maze()
        m.generator = BacktrackingGenerator(width, height)
        m.generate()
        maze = m.tostring()
        splitted = maze.split('\n')

        resizedMaze = []

        for lineIndex in range(len(splitted)):

            line = splitted[lineIndex]

            splittedLine = list(line)

            resizedLine = []

            for charIndex in range(len(splittedLine)):

                char = splittedLine[charIndex]

                if char == '#':

                    resizedLine.append('#')
                    # resizedLine.append('#')
                    # resizedLine.append('#')


                elif char == ' ':

                    resizedLine.append(' ')
                    # resizedLine.append(' ')
                    # resizedLine.append(' ')

            resizedMaze.append(resizedLine)
            # resizedMaze.append(resizedLine)
            # resizedMaze.append(resizedLine)

        for lineIndex in range(len(resizedMaze)):

            line = resizedMaze[lineIndex]

            # splittedLine = list(line)

            for charIndex in range(len(line)):

                char = line[charIndex]

                if char == '#':

                    xCenter = (self.sceneWidth / 2) - ((len(splitted) / 2) * self.gridSize)
                    yCenter = (self.sceneHeight / 2) - ((len(splittedLine) / 2) * self.gridSize)
                    self.addObstructionNode((charIndex * self.gridSize) + xCenter, (lineIndex * self.gridSize) + yCenter)
            QCoreApplication.processEvents()

    def aStarAlgorithm(self):

        # algorithm = AStarAlgorithm(self)
        # algorithm.update.connect(self.printSets)
        # algorithm.finish.connect(self.printPath)
        # algorithm.start()
        # return

        self.stopSimulation = False

        if isinstance(self.startNode, Node) and isinstance(self.endNode, Node):

            self.openSet = []
            self.closedSet = []

            self.openSet.append(self.startNode)

            while len(self.openSet) > 0:

                if self.stopSimulation == True:

                    self.stopSimulation = False

                    return None
                
                # Sort the open list to get the node with the lowest cost first
                self.openSet.sort()

                # Get the node with the lowest cost
                currentNode = self.openSet[0]

                # Remove that node from open set
                self.openSet.remove(currentNode)

                if currentNode != self.startNode and currentNode != self.endNode:

                    # Remove current node from scene
                    self.removeItem(currentNode)

                    # Change current node color
                    currentNode = PinkNode(self, currentNode.position, parent=currentNode.parent)

                    # Add current node to scene
                    self.addItem(currentNode)

                # Add current node to closed set
                self.closedSet.append(currentNode)

                # Check if reached the goal, return the path
                if currentNode == self.endNode:

                    path = []

                    while currentNode != self.startNode:

                        path.append(currentNode.position)
                        currentNode = currentNode.parent

                    # Return reversed path
                    return path[::-1]

                # Get current node position
                (x, y) = currentNode.position

                # Get neighbors
                neighbors = [
                (x, y-self.gridSize),
                (x-self.gridSize, y),
                (x, y+self.gridSize), 
                (x+self.gridSize, y)]
                # neighbors = [
                # (x, y-self.gridSize), 
                # (x-self.gridSize, y-self.gridSize),
                # (x-self.gridSize, y), 
                # (x-self.gridSize, y+self.gridSize),
                # (x, y+self.gridSize), 
                # (x+self.gridSize, y+self.gridSize), 
                # (x+self.gridSize, y), 
                # (x+self.gridSize, y-self.gridSize)]

                # Loop over neighbors
                for (x, y) in neighbors:

                    # Get item at neighbor position
                    item = self.itemAt(x, y, self.view.viewportTransform())

                    # Check if item is wall
                    if isinstance(item, ObstructionNode):

                        continue

                    # Create a neighbor node
                    neighbor = YellowNode(self, (x, y), parent=currentNode)

                    # Check if the neighbor is in the closed list
                    if neighbor in self.closedSet:

                        continue

                    # Generate heuristics (Manhattan distance)
                    neighbor.gCost = self.calculateGcost(neighbor)
                    # neighbor.hCost = max([abs(x - self.endNode.position[0]), abs(y - self.endNode.position[1])])
                    neighbor.hCost = abs(x - self.endNode.position[0]) + abs(y - self.endNode.position[1])
                    neighbor.fCost = neighbor.gCost + neighbor.hCost

                    if neighbor not in self.openSet:

                        self.openSet.append(neighbor)
                        if self.endNode != neighbor:

                            self.addItem(neighbor)

                QCoreApplication.processEvents()

            return None

    def dijkstraAlgorithm(self):

        self.stopSimulation = False

        if isinstance(self.startNode, Node) and isinstance(self.endNode, Node):

            openSet = []
            closedSet = []

            openSet.append(self.startNode)

            while len(openSet) > 0:

                if self.stopSimulation == True:

                    self.stopSimulation = False

                    return None
                
                # Sort the open list to get the node with the lowest cost first
                openSet.sort()

                # Get the node with the lowest cost
                currentNode = openSet[0]

                # Remove that node from open set
                openSet.remove(currentNode)

                if currentNode != self.startNode and currentNode != self.endNode:

                    # Remove current node from scene
                    self.removeItem(currentNode)

                    # Change current node color
                    currentNode = PinkNode(self, currentNode.position, parent=currentNode.parent)

                    # Add current node to scene
                    self.addItem(currentNode)

                # Add current node to closed set
                closedSet.append(currentNode)

                # Check if reached the goal, return the path
                if currentNode == self.endNode:

                    path = []

                    while currentNode != self.startNode:

                        path.append(currentNode.position)
                        currentNode = currentNode.parent

                    # Return reversed path
                    return path[::-1]

                # Get current node position
                (x, y) = currentNode.position

                # Get neighbors
                neighbors = [
                (x, y-self.gridSize),
                (x-self.gridSize, y),
                (x, y+self.gridSize), 
                (x+self.gridSize, y)]
                # neighbors = [
                # (x, y-self.gridSize), 
                # (x-self.gridSize, y-self.gridSize),
                # (x-self.gridSize, y), 
                # (x-self.gridSize, y+self.gridSize),
                # (x, y+self.gridSize), 
                # (x+self.gridSize, y+self.gridSize), 
                # (x+self.gridSize, y), 
                # (x+self.gridSize, y-self.gridSize)]

                # Loop over neighbors
                for (x, y) in neighbors:

                    # Get item at neighbor position
                    item = self.itemAt(x, y, self.view.viewportTransform())

                    # Check if item is wall
                    if isinstance(item, ObstructionNode):

                        continue

                    # Create a neighbor node
                    neighbor = YellowNode(self, (x, y), parent=currentNode)

                    # Check if the neighbor is in the closed list
                    if neighbor in closedSet:

                        continue

                    # Generate heuristics (Manhattan distance)
                    neighbor.gCost = self.calculateGcost(neighbor)
                    neighbor.hCost = 0
                    neighbor.fCost = neighbor.gCost

                    if neighbor not in openSet:

                        openSet.append(neighbor)
                        if self.endNode != neighbor:

                            self.addItem(neighbor)

                QCoreApplication.processEvents()

            return None

    def bestFirstSearchAlgorithm(self):

        self.stopSimulation = False

        if isinstance(self.startNode, Node) and isinstance(self.endNode, Node):

            openSet = []
            closedSet = []

            openSet.append(self.startNode)

            while len(openSet) > 0:

                if self.stopSimulation == True:

                    self.stopSimulation = False

                    return None
                
                # Sort the open list to get the node with the lowest cost first
                openSet.sort()

                # Get the node with the lowest cost
                currentNode = openSet[0]

                # Remove that node from open set
                openSet.remove(currentNode)

                if currentNode != self.startNode and currentNode != self.endNode:

                    # Remove current node from scene
                    self.removeItem(currentNode)

                    # Change current node color
                    currentNode = PinkNode(self, currentNode.position, parent=currentNode.parent)

                    # Add current node to scene
                    self.addItem(currentNode)

                # Add current node to closed set
                closedSet.append(currentNode)

                # Check if reached the goal, return the path
                if currentNode == self.endNode:

                    path = []

                    while currentNode != self.startNode:

                        path.append(currentNode.position)
                        currentNode = currentNode.parent

                    # Return reversed path
                    return path[::-1]

                # Get current node position
                (x, y) = currentNode.position

                # Get neighbors
                neighbors = [
                (x, y-self.gridSize),
                (x-self.gridSize, y),
                (x, y+self.gridSize), 
                (x+self.gridSize, y)]
                # neighbors = [
                # (x, y-self.gridSize), 
                # (x-self.gridSize, y-self.gridSize),
                # (x-self.gridSize, y), 
                # (x-self.gridSize, y+self.gridSize),
                # (x, y+self.gridSize), 
                # (x+self.gridSize, y+self.gridSize), 
                # (x+self.gridSize, y), 
                # (x+self.gridSize, y-self.gridSize)]

                # Loop over neighbors
                for (x, y) in neighbors:

                    # Get item at neighbor position
                    item = self.itemAt(x, y, self.view.viewportTransform())

                    # Check if item is wall
                    if isinstance(item, ObstructionNode):

                        continue

                    # Create a neighbor node
                    neighbor = YellowNode(self, (x, y), parent=currentNode)

                    # Check if the neighbor is in the closed list
                    if neighbor in closedSet:

                        continue

                    # Generate heuristics (Manhattan distance)
                    neighbor.gCost = 0
                    # neighbor.hCost = max([abs(x - self.endNode.position[0]), abs(y - self.endNode.position[1])])
                    neighbor.hCost = abs(x - self.endNode.position[0]) + abs(y - self.endNode.position[1])
                    neighbor.fCost = neighbor.gCost

                    if neighbor not in openSet:

                        openSet.append(neighbor)
                        if self.endNode != neighbor:

                            self.addItem(neighbor)

                QCoreApplication.processEvents()

            return None

    def breadthFirstSearchAlgorithm(self):

        self.stopSimulation = False

        if isinstance(self.startNode, Node) and isinstance(self.endNode, Node):

            openSet = []
            closedSet = []

            openSet.append(self.startNode)

            while len(openSet) > 0:

                if self.stopSimulation == True:

                    self.stopSimulation = False

                    return None
                
                # Sort the open list to get the node with the lowest cost first
                # openSet.sort()

                # Get the node with the lowest cost
                currentNode = openSet[0]

                # Remove that node from open set
                openSet.remove(currentNode)

                if currentNode != self.startNode and currentNode != self.endNode:

                    # Remove current node from scene
                    self.removeItem(currentNode)

                    # Change current node color
                    currentNode = PinkNode(self, currentNode.position, parent=currentNode.parent)

                    # Add current node to scene
                    self.addItem(currentNode)

                # Add current node to closed set
                closedSet.append(currentNode)

                # Check if reached the goal, return the path
                if currentNode == self.endNode:

                    path = []

                    while currentNode != self.startNode:

                        path.append(currentNode.position)
                        currentNode = currentNode.parent

                    # Return reversed path
                    return path[::-1]

                # Get current node position
                (x, y) = currentNode.position

                # Get neighbors
                neighbors = [
                (x, y-self.gridSize),
                (x-self.gridSize, y),
                (x, y+self.gridSize), 
                (x+self.gridSize, y)]
                # neighbors = [
                # (x, y-self.gridSize), 
                # (x-self.gridSize, y-self.gridSize),
                # (x-self.gridSize, y), 
                # (x-self.gridSize, y+self.gridSize),
                # (x, y+self.gridSize), 
                # (x+self.gridSize, y+self.gridSize), 
                # (x+self.gridSize, y), 
                # (x+self.gridSize, y-self.gridSize)]

                # Loop over neighbors
                for (x, y) in neighbors:

                    # Get item at neighbor position
                    item = self.itemAt(x, y, self.view.viewportTransform())

                    # Check if item is wall
                    if isinstance(item, ObstructionNode):

                        continue

                    # Create a neighbor node
                    neighbor = YellowNode(self, (x, y), parent=currentNode)

                    # Check if the neighbor is in the closed list
                    if neighbor in closedSet:

                        continue

                    if neighbor not in openSet:

                        openSet.append(neighbor)
                        if self.endNode != neighbor:

                            self.addItem(neighbor)

                QCoreApplication.processEvents()

            return None

    def depthFirstSearchAlgorithm(self):

        self.stopSimulation = False

        if isinstance(self.startNode, Node) and isinstance(self.endNode, Node):

            openSet = []
            closedSet = []

            openSet.append(self.startNode)

            while len(openSet) > 0:

                if self.stopSimulation == True:

                    self.stopSimulation = False

                    return None
                
                # Sort the open list to get the node with the lowest cost first
                # openSet.sort()

                # Get the node with the lowest cost
                currentNode = openSet[0]

                # Remove that node from open set
                openSet.remove(currentNode)

                if currentNode != self.startNode and currentNode != self.endNode:

                    # Remove current node from scene
                    self.removeItem(currentNode)

                    # Change current node color
                    currentNode = PinkNode(self, currentNode.position, parent=currentNode.parent)

                    # Add current node to scene
                    self.addItem(currentNode)

                # Add current node to closed set
                closedSet.append(currentNode)

                # Check if reached the goal, return the path
                if currentNode == self.endNode:

                    path = []

                    while currentNode != self.startNode:

                        path.append(currentNode.position)
                        currentNode = currentNode.parent

                    # Return reversed path
                    return path[::-1]

                # Get current node position
                (x, y) = currentNode.position

                # Get neighbors
                neighbors = [
                (x, y-self.gridSize),
                (x-self.gridSize, y),
                (x, y+self.gridSize), 
                (x+self.gridSize, y)]
                # neighbors = [
                # (x, y-self.gridSize), 
                # (x-self.gridSize, y-self.gridSize),
                # (x-self.gridSize, y), 
                # (x-self.gridSize, y+self.gridSize),
                # (x, y+self.gridSize), 
                # (x+self.gridSize, y+self.gridSize), 
                # (x+self.gridSize, y), 
                # (x+self.gridSize, y-self.gridSize)]

                neighborSet = []

                # Loop over neighbors
                for (x, y) in neighbors:

                    # Get item at neighbor position
                    item = self.itemAt(x, y, self.view.viewportTransform())

                    # Check if item is wall
                    if isinstance(item, ObstructionNode):

                        continue

                    # Create a neighbor node
                    neighbor = YellowNode(self, (x, y), parent=currentNode)

                    # Check if the neighbor is in the closed list
                    if neighbor in closedSet:

                        continue

                    if neighbor not in openSet:

                        neighborSet.append(neighbor)
                        if self.endNode != neighbor:

                            self.addItem(neighbor)

                for node in openSet:

                    neighborSet.append(node)

                openSet = neighborSet

                QCoreApplication.processEvents()

            return None

    def calculateGcost(self, node):

        (x, y) = node.position

        (pX, pY) = node.parent.position

        gCost = self.gridSize if x == pX or y == pY else (1.4 * self.gridSize)

        return node.parent.gCost + gCost

    def printSets(self):

        

        for node in self.openSet:

            (x, y) = node.position
            item = self.itemAt(x, y, self.view.viewportTransform())
            self.removeItem(item)
            self.addItem(node)
            QCoreApplication.processEvents()

        for node in self.closedSet:

            self.addItem(node)

    def printBluePath(self, path):

        for (x, y) in path[:-1]:

            if self.stopSimulation == True:

                    self.stopSimulation = False

                    break

            item = self.itemAt(x, y, self.view.viewportTransform())
            self.removeItem(item)
            self.addItem(VioletNode(self, (x, y)))
            QCoreApplication.processEvents()

    def printPath(self):

        for (x, y) in self.path[:-1]:

            if self.stopSimulation == True:

                    self.stopSimulation = False

                    break

            item = self.itemAt(x, y, self.view.viewportTransform())
            self.removeItem(item)
            self.addItem(VioletNode(self, (x, y)))
            QCoreApplication.processEvents()