from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

class Line(QGraphicsLineItem):

	def __init__(self, x1, y1, x2, y2, scene, parent = None):

		super().__init__(x1, y1, x2, y2, parent)

		self.scene = scene
