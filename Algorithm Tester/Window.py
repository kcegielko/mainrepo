from PyQt5.QtWidgets import *
from PyQt5.QtOpenGL import *
from PyQt5.QtCore import *

from Scene import Scene
from View import View

class Window(QGLWidget):

    def __init__(self, parent = None):

        super().__init__(parent)

        self.parent = parent

        self.initUi()

    def initUi(self):

        self.layout = QVBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.scene = Scene(self)

        self.view = View(self.scene, self)
        self.layout.addWidget(self.view)

        self.initElements()

        self.show()

    def initElements(self):
        pass

    def clearScene(self):

        self.scene.removeAllNodes()

    def createMaze(self, height, width):

        self.scene.createMaze(width, height)

    def stopAlgorithm(self):

        self.scene.stopSimulation = True

    def aStarAlgorithm(self):

        self.view.aStarAlgorithm()

    def dijkstraAlgorithm(self):

        self.view.dijkstraAlgorithm()

    def bestFirstSearchAlgorithm(self):

        self.view.bestFirstSearchAlgorithm()

    def breadthFirstSearchAlgorithm(self):

        self.view.breadthFirstSearchAlgorithm()

    def depthFirstSearchAlgorithm(self):

        self.view.depthFirstSearchAlgorithm()

    def setTimeElapsed(self, time):

        self.parent.setTimeElapsed(time)