from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from math import *

from Node import Node
from StartNode import StartNode
from EndNode import EndNode
from ObstructionNode import ObstructionNode
from ColorNodes import *

class AStarAlgorithm(QThread):

	finish = pyqtSignal()
	
	update = pyqtSignal()

	def __init__(self, parent = None):

		super().__init__(parent)

		self.scene = parent

	def run(self):

		self.scene.path = self.runAlgorithm()

		self.finish.emit()

	def runAlgorithm(self):

		self.scene.stopSimulation = False

		if isinstance(self.scene.startNode, Node) and isinstance(self.scene.endNode, Node):

			self.scene.openSet = []
			self.scene.closedSet = []

			self.scene.openSet.append(self.scene.startNode)

			while len(self.scene.openSet) > 0:

				if self.scene.stopSimulation == True:

					self.scene.stopSimulation = False

					return None
                
                # Sort the open list to get the node with the lowest cost first
				self.scene.openSet.sort()

                # Get the node with the lowest cost
				currentNode = self.scene.openSet[0]

                # Remove that node from open set
				self.scene.openSet.remove(currentNode)

				if currentNode != self.scene.startNode and currentNode != self.scene.endNode:

                    # Change current node color
					currentNode = PinkNode(self.scene, currentNode.position, parent=currentNode.parent)

                # Add current node to closed set
				self.scene.closedSet.append(currentNode)

                # Check if reached the goal, return the path
				if currentNode == self.scene.endNode:

					path = []

					while currentNode != self.scene.startNode:

						path.append(currentNode.position)
						currentNode = currentNode.parent

                    # Return reversed path
					return path[::-1]

                # Get current node position
				(x, y) = currentNode.position

                # Get neighbors
				neighbors = [
				(x, y-self.scene.gridSize),
				(x-self.scene.gridSize, y),
				(x, y+self.scene.gridSize), 
				(x+self.scene.gridSize, y)]
                # neighbors = [
                # (x-self.gridSize, y), 
                # (x+self.gridSize, y), 
                # (x, y-self.gridSize), 
                # (x, y+self.gridSize), 
                # (x+self.gridSize, y+self.gridSize), 
                # (x+self.gridSize, y-self.gridSize), 
                # (x-self.gridSize, y+self.gridSize), 
                # (x-self.gridSize, y-self.gridSize)]

                # Loop over neighbors
				for (x, y) in neighbors:

                    # Get item at neighbor position
					item = self.scene.itemAt(x, y, self.scene.view.viewportTransform())

                    # Check if item is wall
					if isinstance(item, ObstructionNode):

						continue

                    # Create a neighbor node
					neighbor = YellowNode(self.scene, (x, y), parent=currentNode)

                    # Check if the neighbor is in the closed list
					if neighbor in self.scene.closedSet:

						continue

                    # Generate heuristics (Manhattan distance)
					neighbor.gCost = self.scene.calculateGcost(neighbor)
                    # neighbor.hCost = max([abs(x - self.scene.endNode.position[0]), abs(y - self.scene.endNode.position[1])])
					neighbor.hCost = abs(x - self.scene.endNode.position[0]) + abs(y - self.scene.endNode.position[1])
					neighbor.fCost = neighbor.gCost + neighbor.hCost

					if neighbor not in self.scene.openSet:

						self.scene.openSet.append(neighbor)

				self.update.emit()

			return None