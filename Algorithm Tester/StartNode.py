from Node import Node

class StartNode(Node):

	def __init__(self, scene, position, parent = None):

		super().__init__(scene, position, parent, startNode=True)
