from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from math import *

class Node(QGraphicsItem):

    cornerRadius = 1
    width = 0
    height = 0
    pen = QPen(QColor('#FF343a40'))
    brush = QBrush(QColor('#FF495057'))

    def __init__(self, scene, position, parent = None, startNode = False, endNode = False, currentNode = False, color=None, fCost = inf, gCost = inf, hCost = inf):

        super().__init__(None)

        self.scene = scene
        self.parent = parent
        self.startNode = startNode
        self.endNode = endNode
        self.currentNode = currentNode
        self.position = position
        self.fCost = fCost
        self.gCost = gCost
        self.hCost = hCost

        self.setZValue(0)

        if self.startNode == True:

            self.pen = QPen(QColor('#FF06D6A0'))
            self.brush = QBrush(QColor('#FF06D6A0'))

        elif self.endNode == True:

            self.pen = QPen(QColor('#FFEF476F'))
            self.brush = QBrush(QColor('#FFEF476F'))

        elif self.currentNode == True:

            self.pen = QPen(QColor('#FF118AB2'))
            self.brush = QBrush(QColor('#FF118AB2'))

        elif color != None:

            self.pen = QPen(QColor(color))
            self.brush = QBrush(QColor(color))
            # self.setZValue(-1)

        self.pen.setWidth(0)

        self.width = scene.gridSize
        self.height = scene.gridSize

        self.setPos(position[0], position[1])

        self.setTransformOriginPoint(position[0] + self.width / 2, position[0] + self.height / 2)

    def boundingRect(self):
        return QRectF( 0, 0, self.width, self.height).normalized()

    def paint(self, painter: QPainter, option: QStyleOptionGraphicsItem, widget = None):

        painter.setClipRect(option.exposedRect)

        # outline
        outline = QPainterPath()
        outline.addRoundedRect(0.2, 0.2, self.width - 0.4, self.height - 0.4, self.cornerRadius, self.cornerRadius)

        painter.setPen(self.pen)
        painter.setBrush(self.brush)
        painter.drawPath(outline.simplified())

    def __eq__(self, other):

        return self.position == other.position

    def __lt__(self, other):

        if self.fCost != other.fCost:
            return self.fCost < other.fCost

        else:

            if self.gCost != other.gCost:
                return self.gCost < other.gCost

            else:
                return self.hCost < other.hCost