from math import *

from Node import Node
class BlankNode(Node):

	def __init__(self, scene, position, parent = None, fCost = inf, gCost = inf, hCost = inf):

		super().__init__(scene, position, parent, color='#FFFFFFFF', fCost=fCost, gCost = inf, hCost = inf)

class YellowNode(Node):

	def __init__(self, scene, position, parent = None, fCost = inf, gCost = inf, hCost = inf):

		super().__init__(scene, position, parent, color='#FFFFD166', fCost=fCost, gCost = inf, hCost = inf)

class BlueNode(Node):

	def __init__(self, scene, position, parent = None, fCost = inf, gCost = inf, hCost = inf):

		super().__init__(scene, position, parent, color='#FF073B4C', fCost=fCost, gCost = inf, hCost = inf)

class VioletNode(Node):

	def __init__(self, scene, position, parent = None, fCost = inf, gCost = inf, hCost = inf):

		super().__init__(scene, position, parent, color='#FF9B5DE5', fCost=fCost, gCost = inf, hCost = inf)

class PinkNode(Node):

	def __init__(self, scene, position, parent = None, fCost = inf, gCost = inf, hCost = inf):

		super().__init__(scene, position, parent, color='#FFFF5D8F', fCost=fCost, gCost = inf, hCost = inf)

		