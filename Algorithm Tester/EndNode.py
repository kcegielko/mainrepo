from Node import Node

class EndNode(Node):

	def __init__(self, scene, position, parent = None):

		super().__init__(scene, position, parent, endNode=True)
