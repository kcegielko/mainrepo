from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtOpenGL import *
from PyQt5.QtCore import *
from math import *
from time import sleep, time
from numpy import *

from Line import Line
from Node import Node
from StartNode import StartNode
from EndNode import EndNode
from ObstructionNode import ObstructionNode
from ColorNodes import *

class View(QGraphicsView):

    zoomInFactor = 1.25
    zoom = 5
    zoomStep = 1
    zoomRange = [1, 10]
    zoomClamp = True

    last_x = None
    last_y = None

    addStartNode = False
    addEndNode = False

    def __init__(self, scene, parent = None):

        super().__init__(parent)

        self.scene = scene

        self.scene.view = self

        self.window = parent

        self.initUi()

        self.setScene(self.scene)

        # self.setViewport(QGLWidget())

    def initUi(self):
        self.setRenderHints(QPainter.Antialiasing | QPainter.HighQualityAntialiasing | QPainter.TextAntialiasing | QPainter.SmoothPixmapTransform)
        # self.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        # self.setDragMode(QGraphicsView.RubberBandDrag)
        self.setMouseTracking(True)

    def mousePressEvent(self, event: QMouseEvent):

        if event.button() == Qt.MiddleButton:
            self.middleMouseButtonPress(event)
        elif event.button() == Qt.LeftButton:
            self.leftMouseButtonPress(event)
        elif event.button() == Qt.RightButton:
            self.rightMouseButtonPress(event)

    def mouseReleaseEvent(self, event: QMouseEvent):

        if event.button() == Qt.MiddleButton:
            self.middleMouseButtonRelease(event)
        elif event.button() == Qt.LeftButton:
            self.leftMouseButtonRelease(event)
        elif event.button() == Qt.RightButton:
            self.rightMouseButtonRelease(event)

    def mouseMoveEvent(self, event: QMouseEvent):

        super().mouseMoveEvent(event)
        
        pos = self.mapToScene(event.pos())
        
        if event.buttons() == Qt.RightButton:

            self.scene.removeNode(pos.x(), pos.y())

        if self.addStartNode == False and self.addEndNode == False:

            # FIRST POINT
            if self.last_x is None or self.last_y is None:
                return

            if event.buttons() == Qt.LeftButton:

                self.scene.addItem(Line(self.last_x, self.last_y, pos.x(), pos.y(), self.scene))

                self.last_x = pos.x()
                self.last_y = pos.y()

        

    def middleMouseButtonPress(self, event: QMouseEvent):
        releaseEvent = QMouseEvent(QEvent.MouseButtonRelease, event.localPos(), event.screenPos(), Qt.LeftButton, Qt.NoButton, event.modifiers())
        super().mouseReleaseEvent(releaseEvent)
        self.setDragMode(QGraphicsView.ScrollHandDrag)
        pressEvent = QMouseEvent(event.type(), event.localPos(), event.screenPos(), Qt.LeftButton, event.buttons()| Qt.NoButton, event.modifiers())
        super().mousePressEvent(pressEvent)

    def leftMouseButtonPress(self, event: QMouseEvent):

        x = self.mapToScene(event.pos()).x()
        y = self.mapToScene(event.pos()).y()

        # START NODE
        if self.addStartNode == True:

            self.scene.addStartNode(x, y)

        # END NODE
        elif self.addEndNode == True:

            self.scene.addEndNode(x, y)

        # WALL
        else:

            pos = self.mapToScene(event.pos())

            self.last_x = pos.x()
            self.last_y = pos.y()

        super().mousePressEvent(event)

    def rightMouseButtonPress(self, event: QMouseEvent):
        super().mousePressEvent(event)

    def middleMouseButtonRelease(self, event: QMouseEvent):
        releaseEvent = QMouseEvent(event.type(), event.localPos(), event.screenPos(), Qt.LeftButton, event.buttons() & ~Qt.LeftButton, event.modifiers())
        super().mouseReleaseEvent(releaseEvent)
        self.setDragMode(QGraphicsView.NoDrag)

    def leftMouseButtonRelease(self, event: QMouseEvent):

        if self.addStartNode == True or self.addEndNode == True:

            self.addStartNode = False
            self.addEndNode = False

        else:

            # LAST NODE
            x = self.mapToScene(event.pos()).x()
            y = self.mapToScene(event.pos()).y()

            self.scene.addObstructionNode(x, y)

            # NODES FROM LINE
            items = self.scene.items()

            for item in items:

                if isinstance(item, Line):

                    for i in range(0, 30):

                        point = item.line().pointAt(i / 30)

                        x = point.x()
                        y = point.y()

                        self.scene.addObstructionNode(x, y)

            # REMOVE LINES
            self.scene.removeLines()

            # REMOVE DUPLICATED NODES
            self.scene.removeDuplicatedNodes()

            self.last_x = None
            self.last_y = None

        super().mouseReleaseEvent(event)

    def rightMouseButtonRelease(self, event: QMouseEvent):

        pos = self.mapToScene(event.pos())
        self.scene.removeNode(pos.x(), pos.y())

        super().mouseReleaseEvent(event)

    def wheelEvent(self, event: QWheelEvent):

        zoomOutFactor = 1 / self.zoomInFactor

        if event.angleDelta().y() > 0:
            zoomFactor = self.zoomInFactor
            self.zoom += self.zoomStep
        else:
            zoomFactor = zoomOutFactor
            self.zoom -= self.zoomStep

        clamped = False
        if self.zoom < self.zoomRange[0]:
            self.zoom = self.zoomRange[0]
            clamped = True
        if self.zoom > self.zoomRange[1]:
            self.zoom = self.zoomRange[1]
            clamped = True

        if not clamped or self.zoomClamp is False:
            self.scale(zoomFactor, zoomFactor)

    def keyPressEvent(self, event: QKeyEvent):

        super().keyPressEvent(event)

    def aStarAlgorithm(self):

        self.setTimeElapsed(0)

        startTime = time()

        self.scene.removeColorNodes()

        path = self.scene.aStarAlgorithm()

        if path is not None:
            
            self.scene.printBluePath(path)

        endTime = time()

        self.setTimeElapsed(endTime - startTime)

    def dijkstraAlgorithm(self):

        self.setTimeElapsed(0)

        startTime = time()

        self.scene.removeColorNodes()

        path = self.scene.dijkstraAlgorithm()

        if path is not None:
            
            self.scene.printBluePath(path)

        endTime = time()

        self.setTimeElapsed(endTime - startTime)

    def bestFirstSearchAlgorithm(self):

        self.setTimeElapsed(0)

        startTime = time()

        self.scene.removeColorNodes()

        path = self.scene.bestFirstSearchAlgorithm()

        if path is not None:
            
            self.scene.printBluePath(path)

        endTime = time()

        self.setTimeElapsed(endTime - startTime)

    def breadthFirstSearchAlgorithm(self):

        self.setTimeElapsed(0)

        startTime = time()

        self.scene.removeColorNodes()

        path = self.scene.breadthFirstSearchAlgorithm()

        if path is not None:
            
            self.scene.printBluePath(path)

        endTime = time()

        self.setTimeElapsed(endTime - startTime)

    def depthFirstSearchAlgorithm(self):

        self.setTimeElapsed(0)

        startTime = time()

        self.scene.removeColorNodes()

        path = self.scene.depthFirstSearchAlgorithm()

        if path is not None:
            
            self.scene.printBluePath(path)

        endTime = time()

        self.setTimeElapsed(endTime - startTime)

    def setTimeElapsed(self, time):

        self.window.setTimeElapsed(str(round(time, 3)))