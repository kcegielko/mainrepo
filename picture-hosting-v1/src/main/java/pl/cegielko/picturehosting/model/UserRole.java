package pl.cegielko.picturehosting.model;

public enum UserRole {

    USER,
    ADMIN,
    SUPER_ADMIN

}
