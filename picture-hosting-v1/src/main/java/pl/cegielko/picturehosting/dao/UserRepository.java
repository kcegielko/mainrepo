package pl.cegielko.picturehosting.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.cegielko.picturehosting.model.User;
import pl.cegielko.picturehosting.model.UserRole;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    Optional<User> findByIdAndRole(Long id, UserRole userRole);

    List<User> findAllByRole(UserRole userRole);

    Boolean existsByUsernameOrUserData_Email(String username, String email);

    @Transactional
    @Modifying
    @Query(value = "UPDATE User u SET u.isEnabled=TRUE WHERE u.username=:username AND u.userData IN (SELECT u FROM UserData ud WHERE ud.email=:email)")
    int enableUserWithUsernameAndEmail(@Param("username") String username, @Param("email") String email);

}
