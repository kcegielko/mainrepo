package pl.cegielko.picturehosting.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.cegielko.picturehosting.model.Picture;

import java.util.List;
import java.util.Optional;

public interface PictureRepository extends JpaRepository<Picture, Long> {

    List<Picture> findAllByFolder_IdAndFolder_User_Id(Long folderId, Long userId);

    Optional<Picture> findByIdAndFolder_IdAndFolder_UserId(Long pictureId, Long folderId, Long userId);

    void deleteByIdAndFolder_Id(Long pictureId, Long folderId);

}
