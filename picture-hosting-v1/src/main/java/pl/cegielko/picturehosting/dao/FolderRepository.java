package pl.cegielko.picturehosting.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.cegielko.picturehosting.model.Folder;

import java.util.List;
import java.util.Optional;

public interface FolderRepository extends JpaRepository<Folder, Long> {

    List<Folder> findAllByUser_Id(Long id);

    Optional<Folder> findByIdAndUser_Id(Long folderId, Long userId);

    void deleteByIdAndUser_Id(Long folderId, Long userId);

}
