package pl.cegielko.picturehosting.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import pl.cegielko.picturehosting.filter.JwtTokenVerifierFilter;
import pl.cegielko.picturehosting.filter.JwtUsernameAndPasswordAuthenticationFilter;
import pl.cegielko.picturehosting.service.UserService;
import pl.cegielko.picturehosting.util.PasswordEncoder;

import static pl.cegielko.picturehosting.model.UserRole.*;

@Configuration
@AllArgsConstructor
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final Environment environment;

    @Bean
    DaoAuthenticationProvider daoAuthenticationProvider(){
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(userService);
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder.bCryptPasswordEncoder());
        return daoAuthenticationProvider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(daoAuthenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/login").permitAll()
                .antMatchers("/api/v1/user/**").hasAnyRole(SUPER_ADMIN.name(), ADMIN.name(), USER.name())
                .antMatchers("/api/v1/admin/**").hasAnyRole(SUPER_ADMIN.name(), ADMIN.name())
                .antMatchers("/api/v1/super-admin/**").hasRole(SUPER_ADMIN.name())
                .antMatchers("/api/v1/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilter(new JwtUsernameAndPasswordAuthenticationFilter(authenticationManager(), environment))
                .addFilterAfter(new JwtTokenVerifierFilter(environment, userService), JwtUsernameAndPasswordAuthenticationFilter.class);
    }
}
