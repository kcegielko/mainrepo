package pl.cegielko.picturehosting.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.cegielko.picturehosting.dao.UserRepository;
import pl.cegielko.picturehosting.model.*;
import pl.cegielko.picturehosting.util.EmailSender;
import pl.cegielko.picturehosting.util.PasswordEncoder;
import pl.cegielko.picturehosting.util.UserBuilder;

import java.util.List;

import static pl.cegielko.picturehosting.model.UserRole.SUPER_ADMIN;
import static pl.cegielko.picturehosting.model.UserRole.USER;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    @Value("${SUPER_ADMIN_USERNAME}")
    private String superAdminUsername;

    @Value("${SUPER_ADMIN_PASSWORD}")
    private String superAdminPassword;

    @Value("${SUPER_ADMIN_EMAIL}")
    private String superAdminEmail;

    @Value("${CONFIRMATION_TOKEN_SECRET_KEY}")
    private String CONFIRMATION_TOKEN_SECRET_KEY;

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final EmailSender emailSender;

    private User getCurrentlyLoggedUser(){
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return loadUserByUsername(username);
    }

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {

        if (username.equals(superAdminUsername)){

            User user = new UserBuilder()
                    .withUsername(superAdminUsername)
                    .withPassword(passwordEncoder.bCryptPasswordEncoder().encode(superAdminPassword))
                    .withRole(SUPER_ADMIN)
                    .withIsEnabled(true)
                    .withIsAccountNonExpired(true)
                    .withIsAccountNonLocked(true)
                    .withIsCredentialsNonExpired(true)
                    .withEmail(superAdminEmail)
                    .build();

            return user;
        }

        return userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("Username not found"));
    }

    public User registerUser(RegistrationCredentials credentials){

        Boolean alreadyExists = userRepository.existsByUsernameOrUserData_Email(credentials.getUsername(), credentials.getEmail());

        if (!alreadyExists){

            User user = new UserBuilder()
                    .withUsername(credentials.getUsername())
                    .withPassword(passwordEncoder.bCryptPasswordEncoder().encode(credentials.getPassword()))
                    .withRole(USER)
                    .withIsEnabled(false)
                    .withIsAccountNonExpired(true)
                    .withIsAccountNonLocked(true)
                    .withIsCredentialsNonExpired(true)
                    .withEmail(credentials.getEmail())
                    .withPhoneNumber(credentials.getPhoneNumber())
                    .withFirstName(credentials.getFirstName())
                    .withLastName(credentials.getLastName())
                    .build();

            emailSender.sendConfirmationEmail(credentials.getEmail(), credentials.getUsername());

            return userRepository.save(user);

        } else {
            throw new IllegalArgumentException("Username or email already exists");
        }
    }

    public int confirmEmail(String token){

        Jws<Claims> claimsJws = Jwts.parserBuilder()
                .setSigningKey(Keys.hmacShaKeyFor(CONFIRMATION_TOKEN_SECRET_KEY.getBytes()))
                .build().parseClaimsJws(token);

        Claims body = claimsJws.getBody();

        String username = body.getSubject();

        String email = (String) body.get("email");

        return userRepository.enableUserWithUsernameAndEmail(username, email);
    }

    public User getUserData() {
        return getCurrentlyLoggedUser();
    }

    public User updateCurrentUserData(UserData userData) {
        User user = getCurrentlyLoggedUser();
        userData.setId(user.getUserData().getId());
        user.setUserData(userData);
        return userRepository.save(user);
    }

    public List<User> getAllNotAdminData() {
        return userRepository.findAllByRole(USER);
    }

    public User updateAnyNotAdminData(UserData userData, Long id) {
        User user = userRepository.findByIdAndRole(id, USER).orElseThrow(() -> new IllegalArgumentException("Invalid user id or invalid user role"));
        userData.setId(user.getUserData().getId());
        user.setUserData(userData);
        return userRepository.save(user);
    }

    public User getAnyNotAdminData(Long id) {
        return userRepository.findByIdAndRole(id, USER).orElseThrow(() -> new IllegalArgumentException("Invalid user id or invalid user role"));
    }

    public List<User> getAllUsersData() {
        return userRepository.findAll();
    }

    public User getAnyUserData(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user id"));
    }

    public User updateAnyUserData(UserData userData, Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user id"));
        userData.setId(user.getUserData().getId());
        user.setUserData(userData);
        return userRepository.save(user);
    }

    public User updateCurrentUserPassword(UserCredentials userCredentials) {
        User user = getCurrentlyLoggedUser();
        user.setPassword(passwordEncoder.bCryptPasswordEncoder().encode(userCredentials.getPassword()));
        return userRepository.save(user);
    }

    public User updateCurrentUserUsername(UserCredentials userCredentials) {
        User user = getCurrentlyLoggedUser();
        user.setUsername(userCredentials.getUsername());
        return userRepository.save(user);
    }

    public User updateAnyNotAdminPassword(UserCredentials userCredentials, Long id) {
        User user = userRepository.findByIdAndRole(id, USER).orElseThrow(() -> new IllegalArgumentException("Invalid user id or invalid user role"));
        user.setPassword(passwordEncoder.bCryptPasswordEncoder().encode(userCredentials.getPassword()));
        return userRepository.save(user);
    }

    public User updateAnyNotAdminUsername(UserCredentials userCredentials, Long id) {
        User user = userRepository.findByIdAndRole(id, USER).orElseThrow(() -> new IllegalArgumentException("Invalid user id or invalid user role"));
        user.setUsername(userCredentials.getUsername());
        return userRepository.save(user);
    }

    public User updateAnyUserUsername(UserCredentials userCredentials, Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user id"));
        user.setUsername(userCredentials.getUsername());
        return userRepository.save(user);
    }

    public User updateAnyUserPassword(UserCredentials userCredentials, Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user id"));
        user.setPassword(passwordEncoder.bCryptPasswordEncoder().encode(userCredentials.getPassword()));
        return userRepository.save(user);
    }

    public User updateAnyUserRole(User user, Long id) {
        User u = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user id"));
        u.setRole(user.getRole());
        return userRepository.save(u);
    }
}