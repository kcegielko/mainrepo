package pl.cegielko.picturehosting.service;

import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.cegielko.picturehosting.dao.PictureRepository;
import pl.cegielko.picturehosting.model.Folder;
import pl.cegielko.picturehosting.model.Picture;
import pl.cegielko.picturehosting.model.User;

import java.util.List;

@Service
@AllArgsConstructor
public class PictureService {

    private final PictureRepository pictureRepository;
    private final UserService userService;
    private final FolderService folderService;

    private User getCurrentlyLoggedUser(){
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userService.loadUserByUsername(username);
    }

    public List<Picture> getPicturesOfCurrentUserInFolder(Long id) {
        return pictureRepository.findAllByFolder_IdAndFolder_User_Id(id, getCurrentlyLoggedUser().getId());
    }

    public Picture getPictureOfCurrentUserInFolder(Long folderId, Long pictureId) {
        return pictureRepository.findByIdAndFolder_IdAndFolder_UserId(pictureId, folderId, getCurrentlyLoggedUser().getId()).orElseThrow(() -> new IllegalArgumentException("Invalid picture id or folder id"));
    }

    public Picture addPictureToCurrentUserFolder(Long id, Picture picture) {
        Folder folder = folderService.getUserFolder(id);
        picture.setFolder(folder);
        return pictureRepository.save(picture);
    }

    public void updatePictureInCurrentUserFolder(Long folderId, Long pictureId, Picture picture) {
        picture.setId(pictureId);
        picture.setFolder(folderService.getUserFolder(folderId));
        pictureRepository.save(picture);
    }

    @Transactional
    public void deletePictureInCurrentUserFolder(Long folderId, Long pictureId) {
        pictureRepository.deleteByIdAndFolder_Id(pictureId, folderId);
    }

}
