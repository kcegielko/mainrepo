package pl.cegielko.picturehosting.service;

import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.cegielko.picturehosting.dao.FolderRepository;
import pl.cegielko.picturehosting.model.Folder;
import pl.cegielko.picturehosting.model.User;
import pl.cegielko.picturehosting.model.UserData;

import java.util.List;

@Service
@AllArgsConstructor
public class FolderService {

    private final FolderRepository folderRepository;
    private final UserService userService;

    private User getCurrentlyLoggedUser(){
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userService.loadUserByUsername(username);
    }

    public List<Folder> getFoldersCurrentUser() {
        return folderRepository.findAllByUser_Id(getCurrentlyLoggedUser().getId());
    }

    public Folder getUserFolder(Long id) {
        return folderRepository.findByIdAndUser_Id(id, getCurrentlyLoggedUser().getId()).orElseThrow(() -> new IllegalArgumentException("Invalid folder id"));
    }

    public Folder addCurrentUserFolder(Folder folder) {
        folder.setUser(getCurrentlyLoggedUser());
        return folderRepository.save(folder);
    }

    public void updateCurrentUserFolder(Folder folder, Long id) {
        folder.setId(id);
        folder.setUser(getCurrentlyLoggedUser());
        folderRepository.save(folder);
    }

    @Transactional
    public void deleteCurrentUserFolder(Long id) {
        folderRepository.deleteByIdAndUser_Id(id, getCurrentlyLoggedUser().getId());
    }
}
