package pl.cegielko.picturehosting.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class PasswordEncoder {

    @Value("${B_CRYPT_PASSWORD_ENCODER_STRENGTH}")
    private int bCryptPasswordEncoderStrength;

    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder(bCryptPasswordEncoderStrength);
    }

}
