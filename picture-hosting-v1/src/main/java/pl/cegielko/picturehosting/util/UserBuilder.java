package pl.cegielko.picturehosting.util;

import pl.cegielko.picturehosting.model.User;
import pl.cegielko.picturehosting.model.UserData;
import pl.cegielko.picturehosting.model.UserRole;

public class UserBuilder implements Builder<User> {

    private User user;
    private UserData userData;

    public UserBuilder() {
        reset();
    }

    @Override
    public void reset() {
        user = new User();
        userData = new UserData();
    }

    @Override
    public User build() {
        User u = user;
        userData.setUser(u);
        u.setUserData(this.userData);
        return u;
    }

    public UserBuilder withId(Long id){

        user.setId(id);
        return this;

    }

    public UserBuilder withUsername(String username){

        user.setUsername(username);
        return this;

    }

    public UserBuilder withPassword(String password){

        user.setPassword(password);
        return this;

    }

    public UserBuilder withRole(UserRole userRole){

        user.setRole(userRole);
        return this;

    }

    public UserBuilder withIsEnabled(boolean isEnabled){

        user.setEnabled(isEnabled);
        return this;

    }

    public UserBuilder withIsCredentialsNonExpired(boolean isCredentialsNonExpired){

        user.setCredentialsNonExpired(isCredentialsNonExpired);
        return this;

    }

    public UserBuilder withIsAccountNonLocked(boolean isAccountNonLocked){

        user.setAccountNonLocked(isAccountNonLocked);
        return this;

    }

    public UserBuilder withIsAccountNonExpired(boolean isAccountNonExpired){

        user.setAccountNonExpired(isAccountNonExpired);
        return this;

    }

    public UserBuilder withFirstName(String firstName){

        userData.setFirstName(firstName);
        return this;

    }

    public UserBuilder withLastName(String lastName){

        userData.setLastName(lastName);
        return this;

    }

    public UserBuilder withEmail(String email){

        userData.setEmail(email);
        return this;

    }

    public UserBuilder withPhoneNumber(String phoneNumber){

        userData.setPhoneNumber(phoneNumber);
        return this;

    }

}
