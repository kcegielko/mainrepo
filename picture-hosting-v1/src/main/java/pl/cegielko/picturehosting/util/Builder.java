package pl.cegielko.picturehosting.util;

public interface Builder<T> {

    void reset();

    T build();

}
