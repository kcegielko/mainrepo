package pl.cegielko.picturehosting.api;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.cegielko.picturehosting.model.*;
import pl.cegielko.picturehosting.service.FolderService;
import pl.cegielko.picturehosting.service.PictureService;
import pl.cegielko.picturehosting.service.UserService;

import javax.websocket.server.PathParam;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/user")
public class UserApiController {

    private final UserService userService;
    private final FolderService folderService;
    private final PictureService pictureService;

    @GetMapping(path = "/hello")
    private String hello(){
        return "Hello World!";
    }

    // GET THIS USER DATA

    @GetMapping(path = "/data")
    private User getCurrentUserData(){
        return userService.getUserData();
    }

    // UPDATE USER USERNAME

    @PutMapping(path = "/update/username")
    private User updateCurrentUserUsername(@RequestBody UserCredentials UserCredentials){
        return userService.updateCurrentUserUsername(UserCredentials);
    }

    // UPDATE USER PASSWORD

    @PutMapping(path = "/update/password")
    private User updateCurrentUserPassword(@RequestBody UserCredentials UserCredentials){
        return userService.updateCurrentUserPassword(UserCredentials);
    }

    // UPDATE USER DATA

    @PutMapping(path = "/update/data")
    private User updateCurrentUserData(@RequestBody UserData userData){
        return userService.updateCurrentUserData(userData);
    }

    // GET ALL USER FOLDERS

    @GetMapping(path = "/folders")
    private List<Folder> getFoldersCurrentUser(){
        return folderService.getFoldersCurrentUser();
    }

    // ADD FOLDER

    @PostMapping(path = "/folder")
    private Folder addCurrentUserFolder(@RequestBody Folder folder){
        return folderService.addCurrentUserFolder(folder);
    }

    // UPDATE USER FOLDER

    @PutMapping(path = "/folder/{id}")
    private void updateCurrentUserFolder(@RequestBody Folder folder, @PathVariable(name = "id") Long id){
        folderService.updateCurrentUserFolder(folder, id);
    }

    // DELETE USER FOLDER

    @DeleteMapping(path = "/folder/{id}")
    private void deleteCurrentUserFolder(@PathVariable(name = "id") Long id){
        folderService.deleteCurrentUserFolder(id);
    }

    // GET ALL IMAGES USER IN FOLDER

    @GetMapping(path = "/folder/{id}")
    private List<Picture> getPicturesOfCurrentUserInFolder(@PathVariable(name = "id") Long id){
        return pictureService.getPicturesOfCurrentUserInFolder(id);
    }

    // GET IMAGE IN USER FOLDER

    @GetMapping(path = "/folder/{id}", params = "pictureId")
    private Picture getPictureOfCurrentUserInFolder(@PathVariable(name = "id") Long folderId, @PathParam(value = "imageId") Long pictureId){
        return pictureService.getPictureOfCurrentUserInFolder(folderId, pictureId);
    }

    // ADD IMAGE TO USER FOLDER

    @PostMapping(path = "/folder/{id}")
    private Picture addPictureToCurrentUserFolder(@PathVariable(name = "id") Long id, @RequestBody Picture picture){
        return pictureService.addPictureToCurrentUserFolder(id, picture);
    }

    // UPDATE IMAGE IN USER FOLDER

    @PutMapping(path = "/folder/{id}", params = "pictureId")
    private void updatePictureInCurrentUserFolder(@PathVariable(name = "id") Long folderId, @PathParam(value = "imageId") Long pictureId, @RequestBody Picture picture){
        pictureService.updatePictureInCurrentUserFolder(folderId, pictureId, picture);
    }

    // DELETE IMAGE FROM USER FOLDER

    @DeleteMapping(path = "/folder/{id}", params = "pictureId")
    private void deletePictureInCurrentUserFolder(@PathVariable(name = "id") Long folderId, @PathParam(value = "imageId") Long pictureId){
        pictureService.deletePictureInCurrentUserFolder(folderId, pictureId);
    }
}