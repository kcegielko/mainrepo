package pl.cegielko.picturehosting.api;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.cegielko.picturehosting.model.RegistrationCredentials;
import pl.cegielko.picturehosting.model.User;
import pl.cegielko.picturehosting.service.UserService;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class PublicApiController {

    private final UserService userService;

    @GetMapping(path = "/hello")
    private String hello(){
        return "Hello World!";
    }

    // REGISTER

    @PostMapping(path = "/register")
    private User registerUser(@RequestBody RegistrationCredentials registrationCredentials){
        return userService.registerUser(registrationCredentials);
    }

    // CONFIRM EMAIL

    @GetMapping(path = "/confirm", params = "token")
    private int confirmEmail(@RequestParam("token") String token){
        return userService.confirmEmail(token);
    }

}
