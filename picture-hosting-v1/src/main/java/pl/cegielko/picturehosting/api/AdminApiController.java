package pl.cegielko.picturehosting.api;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.cegielko.picturehosting.model.User;
import pl.cegielko.picturehosting.model.UserCredentials;
import pl.cegielko.picturehosting.model.UserData;
import pl.cegielko.picturehosting.service.UserService;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/admin")
public class AdminApiController {

    private final UserService userService;

    @GetMapping(path = "/hello")
    private String hello(){
        return "Hello World!";
    }

    // GET ALL USERS (NOT ADMINS) DATA

    @GetMapping(path = "/data")
    private List<User> getAllNotAdminsData(){
        return userService.getAllNotAdminData();
    }

    // GET ANY USER (NOT ADMIN) DATA

    @GetMapping(path = "/data/{id}")
    private User getAnyNotAdminData(@PathVariable(name = "id") Long id){
        return userService.getAnyNotAdminData(id);
    }

    // UPDATE ANY USER (NOT ADMIN) USERNAME

    @PutMapping(path = "/update/username/{id}")
    private User updateAnyNotAdminUsername(@RequestBody UserCredentials userCredentials, @PathVariable(name = "id") Long id){
        return userService.updateAnyNotAdminUsername(userCredentials, id);
    }

    // UPDATE ANY USER (NOT ADMIN) PASSWORD

    @PutMapping(path = "/update/password/{id}")
    private User updateAnyNotAdminPassword(@RequestBody UserCredentials userCredentials, @PathVariable(name = "id") Long id){
        return userService.updateAnyNotAdminPassword(userCredentials, id);
    }

    // UPDATE ANY USER (NOT ADMIN) DATA

    @PutMapping(path = "/update/data/{id}")
    private User updateAnyNotAdminData(@RequestBody UserData userData, @PathVariable(name = "id") Long id){
        return userService.updateAnyNotAdminData(userData, id);
    }

}
