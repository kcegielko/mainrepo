package pl.cegielko.picturehosting.api;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.cegielko.picturehosting.model.User;
import pl.cegielko.picturehosting.model.UserCredentials;
import pl.cegielko.picturehosting.model.UserData;
import pl.cegielko.picturehosting.service.UserService;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/super-admin")
public class SuperAdminApi {

    private final UserService userService;

    @GetMapping(path = "/hello")
    private String hello(){
        return "Hello World!";
    }

    // GET ALL USERS DATA

    @GetMapping(path = "/data")
    private List<User> getAllUsersData(){
        return userService.getAllUsersData();
    }

    // GET ANY USER DATA

    @GetMapping(path = "/data/{id}")
    private User getAnyUserData(@PathVariable(name = "id") Long id){
        return userService.getAnyUserData(id);
    }

    // UPDATE ANY USER USERNAME

    @PutMapping(path = "/update/username/{id}")
    private User updateAnyUserUsername(@RequestBody UserCredentials userCredentials, @PathVariable(name = "id") Long id){
        return userService.updateAnyUserUsername(userCredentials, id);
    }

    // UPDATE ANY USER PASSWORD

    @PutMapping(path = "/update/password/{id}")
    private User updateAnyUserPassword(@RequestBody UserCredentials userCredentials, @PathVariable(name = "id") Long id){
        return userService.updateAnyUserPassword(userCredentials, id);
    }

    // UPDATE ANY USER ROLE

    @PutMapping(path = "/update/role/{id}")
    private User updateAnyUserRole(@RequestBody User user, @PathVariable(name = "id") Long id){
        return userService.updateAnyUserRole(user, id);
    }

    // UPDATE ANY USER DATA

    @PutMapping(path = "/update/data/{id}")
    private User updateAnyUserData(@RequestBody UserData userData, @PathVariable(name = "id") Long id){
        return userService.updateAnyUserData(userData, id);
    }
}
