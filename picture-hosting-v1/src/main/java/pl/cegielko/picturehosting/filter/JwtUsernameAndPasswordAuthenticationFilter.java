package pl.cegielko.picturehosting.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.SneakyThrows;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import pl.cegielko.picturehosting.model.UserCredentials;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Objects;

public class JwtUsernameAndPasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final String JWT_SECRET_KEY;

    private final int JWT_EXPIRATION_DAYS;

    public JwtUsernameAndPasswordAuthenticationFilter(AuthenticationManager authenticationManager, Environment environment) {
        super(authenticationManager);
        this.JWT_SECRET_KEY = environment.getProperty("JWT_SECRET_KEY");
        this.JWT_EXPIRATION_DAYS = Integer.parseInt(Objects.requireNonNull(environment.getProperty("JWT_EXPIRATION_DAYS")));
    }

    @SneakyThrows
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {

        UserCredentials userCredentials = new ObjectMapper()
                .readValue(
                        request.getInputStream(), // Read body
                        UserCredentials.class); // Map it to UserCredentials class object

        return getAuthenticationManager()
                .authenticate(new UsernamePasswordAuthenticationToken( // Create authentication token
                userCredentials.getUsername(), // With username
                userCredentials.getPassword() // With password
        ));
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        String role = authResult.getAuthorities().stream().findFirst().orElseThrow(() -> new RuntimeException("User does not exists")).getAuthority();

        String token = Jwts.builder()
                .setSubject(authResult.getName())
                .claim("role", role)
                .setIssuedAt(Date.valueOf(LocalDate.now()))
                .setExpiration(Date.valueOf(LocalDate.now().plusDays(JWT_EXPIRATION_DAYS)))
                .signWith(Keys.hmacShaKeyFor(JWT_SECRET_KEY.getBytes()))
                .compact();

        response.addHeader("Authorization", "Bearer " + token);
    }
}
