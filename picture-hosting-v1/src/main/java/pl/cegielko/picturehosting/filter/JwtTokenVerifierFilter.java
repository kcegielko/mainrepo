package pl.cegielko.picturehosting.filter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import org.springframework.core.env.Environment;
import pl.cegielko.picturehosting.model.User;
import pl.cegielko.picturehosting.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

public class JwtTokenVerifierFilter extends OncePerRequestFilter {

    private final String JWT_SECRET_KEY;
    private final UserService userService;

    public JwtTokenVerifierFilter(Environment environment, UserService userService) {
        this.JWT_SECRET_KEY = environment.getProperty("JWT_SECRET_KEY");
        this.userService = userService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

        String authorizationHeader = httpServletRequest.getHeader("Authorization");

        if (Strings.isNotBlank(authorizationHeader) && Strings.isNotEmpty(authorizationHeader) && authorizationHeader.startsWith("Bearer ")){

            String token = authorizationHeader.replace("Bearer ", "");

            Jws<Claims> claimsJws = Jwts.parserBuilder()
                    .setSigningKey(Keys.hmacShaKeyFor(JWT_SECRET_KEY.getBytes()))
                    .build().parseClaimsJws(token);

            Claims body = claimsJws.getBody();

            String username = body.getSubject();

            String role = (String) body.get("role");

            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                    username,
                    null,
                    Set.of(new SimpleGrantedAuthority(role))
            );

            usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));

            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);

    }
}
