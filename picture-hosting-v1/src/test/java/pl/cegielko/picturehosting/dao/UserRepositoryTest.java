package pl.cegielko.picturehosting.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import pl.cegielko.picturehosting.model.User;
import pl.cegielko.picturehosting.model.UserRole;
import pl.cegielko.picturehosting.util.UserBuilder;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@DataJpaTest
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    void itShouldFindUserByUsername() {

        // given
        UserBuilder userBuilder = new UserBuilder();
        User user = userBuilder
                .withId(1L)
                .withUsername("username")
                .withPassword("password")
                .withRole(UserRole.USER)
                .withIsEnabled(true)
                .withIsAccountNonExpired(true)
                .withIsAccountNonLocked(true)
                .withIsCredentialsNonExpired(true)
                .withEmail("email@example.com")
                .withFirstName("FirstName")
                .withLastName("LastName")
                .withPhoneNumber("123456789")
                .build();

        userRepository.save(user);

        // when
        User userByUsername = userRepository.findByUsername("username").orElse(null);

        // then
        assertThat(userByUsername).isEqualTo(user);
    }

    @Test
    void itShouldNotFindUserByUsername() {

        // when
        User userByUsername = userRepository.findByUsername("username").orElse(null);

        // then
        assertThat(userByUsername).isNull();
    }
}