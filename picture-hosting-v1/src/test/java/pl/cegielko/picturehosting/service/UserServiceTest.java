package pl.cegielko.picturehosting.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.util.ReflectionTestUtils;
import pl.cegielko.picturehosting.dao.FolderRepository;
import pl.cegielko.picturehosting.dao.PictureRepository;
import pl.cegielko.picturehosting.dao.UserRepository;
import pl.cegielko.picturehosting.model.User;
import pl.cegielko.picturehosting.model.UserRole;
import pl.cegielko.picturehosting.util.EmailSender;
import pl.cegielko.picturehosting.util.PasswordEncoder;
import pl.cegielko.picturehosting.util.UserBuilder;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;
    private UserService userService;
    private PasswordEncoder passwordEncoder;
    private EmailSender emailSender;

    @BeforeEach
    void setUp() {
        userService = new UserService(userRepository, passwordEncoder, emailSender);
    }

    @Test
    void itShouldLoadUserByUsername() {

        // given
        UserBuilder userBuilder = new UserBuilder();
        User user = userBuilder
                .withId(1L)
                .withUsername("username")
                .withPassword("password")
                .withRole(UserRole.USER)
                .withIsEnabled(true)
                .withIsAccountNonExpired(true)
                .withIsAccountNonLocked(true)
                .withIsCredentialsNonExpired(true)
                .withEmail("email@example.com")
                .withFirstName("FirstName")
                .withLastName("LastName")
                .withPhoneNumber("123456789")
                .build();

        given(userRepository.findByUsername("username")).willReturn(java.util.Optional.of(user));

        // when
        userService.loadUserByUsername("username");

        // then
        verify(userRepository).findByUsername("username");

    }

    @Test
    void itShouldNotLoadUserByUsername() {

        // given
        // when
        // then
        assertThatThrownBy(() -> userService.loadUserByUsername(""))
                .isInstanceOf(UsernameNotFoundException.class)
                .hasMessageContaining("Username not found");

    }

    @Test
    void itShouldLoadSuperAdminUsername() {

        // given
        UserBuilder userBuilder = new UserBuilder();
        User user = userBuilder
                .withId(1L)
                .withUsername("superAdmin")
                .withPassword("superAdmin")
                .withRole(UserRole.SUPER_ADMIN)
                .withIsEnabled(true)
                .withIsAccountNonExpired(true)
                .withIsAccountNonLocked(true)
                .withIsCredentialsNonExpired(true)
                .withEmail("superAdmin@example.com")
                .withFirstName("Super")
                .withLastName("Admin")
                .withPhoneNumber("666666666")
                .build();

        ReflectionTestUtils.setField(userService, "superAdminUsername", "superAdmin");
        ReflectionTestUtils.setField(userService, "superAdminPassword", "superAdmin");
        ReflectionTestUtils.setField(userService, "superAdminEmail", "superAdmin@example.com");

        // when
        // then
        assertThat(userService.loadUserByUsername("superAdmin")).isEqualTo(user);

    }
}