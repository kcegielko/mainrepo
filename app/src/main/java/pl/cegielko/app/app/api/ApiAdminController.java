package pl.cegielko.app.app.api;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.cegielko.app.app.models.Geolocation;
import pl.cegielko.app.app.services.GeolocationService;

@RestController
@RequestMapping(value = "/api/admin")
@AllArgsConstructor
public class ApiAdminController {

    private final GeolocationService geolocationService;

    @PostMapping(path = "/add")
    private Geolocation addGeolocation(@RequestBody Geolocation geolocation){
        return geolocationService.addGeolocation(geolocation);
    }

    @PostMapping(path = "/add", params = {"ip", "url", "longitude", "latitude"})
    private Geolocation addGeolocation(@RequestParam("ip") String ip, @RequestParam("url") String url, @RequestParam("longitude") String longitude, @RequestParam("latitude") String latitude){
        return geolocationService.addGeolocation(new Geolocation(longitude, latitude, ip, url));
    }

    @PutMapping(path = "/update", params = {"id"})
    private Geolocation updateGeolocation(@RequestParam("id") Long id, @RequestBody Geolocation geolocation){
        return geolocationService.updateGeolocation(id, geolocation);
    }

    @DeleteMapping(path = "/delete", params = {"id"})
    private void removeGeolocation(@RequestParam("id") Long id){
        geolocationService.removeGeolocation(id);
    }
}
