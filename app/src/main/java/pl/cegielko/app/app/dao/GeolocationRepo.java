package pl.cegielko.app.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.cegielko.app.app.models.Geolocation;

import java.util.Optional;

@Repository
public interface GeolocationRepo extends JpaRepository<Geolocation, Long> {

    public Optional<Geolocation> findByUrl(String url);

    public Optional<Geolocation> findByIp(String ip);

    @Transactional
    @Modifying
    @Query("UPDATE Geolocation g " +
            "SET g.latitude = ?2, " +
            "g.longitude = ?3, " +
            "g.ip = ?4, " +
            "g.url = ?5 " +
            "WHERE g.id = ?1")
    public int updateGeolocation(Long id, String latitude, String longitude, String ip, String url);

}
