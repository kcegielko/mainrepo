package pl.cegielko.app.security.api;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import pl.cegielko.app.security.models.AuthenticationRequest;
import pl.cegielko.app.security.models.AuthenticationResponse;
import pl.cegielko.app.security.models.RegistrationRequest;
import pl.cegielko.app.security.models.User;
import pl.cegielko.app.security.services.RegistrationService;
import pl.cegielko.app.security.services.UserService;
import pl.cegielko.app.security.utility.Jwt;

@RestController
@RequestMapping(path = "/api/security")
@AllArgsConstructor
public class SecurityController {

    private final UserService userService;
    private final RegistrationService registrationService;
    private final Jwt jwt;
    private final AuthenticationManager authenticationManager;

    @PostMapping(path = "/register/user")
    private String registerUser(@RequestBody RegistrationRequest request){
        return registrationService.registerUser(request);
    }

    @GetMapping(path = "/confirm")
    private String confirm(@RequestParam("token") String token){
        return registrationService.confirmToken(token);
    }

    @PostMapping(path = "/authorize")
    private ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest){

        try {

            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword()));

        } catch (BadCredentialsException e){

            throw new BadCredentialsException("Incorrect username or password", e);

        }

        final User user = (User) userService.loadUserByUsername(authenticationRequest.getUsername());

        final String jwtToken = jwt.generateToken(user);

        return ResponseEntity.ok(new AuthenticationResponse(jwtToken));

    }

}
