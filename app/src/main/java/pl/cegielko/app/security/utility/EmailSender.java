package pl.cegielko.app.security.utility;

public interface EmailSender {

    void sendEmail(String destination, String email);

}
