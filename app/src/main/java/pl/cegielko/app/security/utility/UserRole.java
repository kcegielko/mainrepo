package pl.cegielko.app.security.utility;

public enum UserRole {
    USER,
    ADMIN
}
