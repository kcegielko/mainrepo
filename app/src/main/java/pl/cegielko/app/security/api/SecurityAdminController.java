package pl.cegielko.app.security.api;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.cegielko.app.app.models.Geolocation;
import pl.cegielko.app.security.models.RegistrationRequest;
import pl.cegielko.app.security.models.User;
import pl.cegielko.app.security.services.RegistrationService;
import pl.cegielko.app.security.services.UserService;

import java.util.List;

@RestController
@RequestMapping(path = "/api/admin/security")
@AllArgsConstructor
public class SecurityAdminController {

    private final UserService userService;
    private final RegistrationService registrationService;

    @GetMapping(path = "/users")
    private List<User> getAllUsers(){
        return userService.getAllUsers();
    }

    @PostMapping(path = "/register/admin")
    private String registerAdmin(@RequestBody RegistrationRequest request){
        return registrationService.registerAdmin(request);
    }

    @PostMapping(path = "/register/user")
    private String registerUser(@RequestBody RegistrationRequest request){
        return registrationService.registerUser(request);
    }

    @PutMapping(path = "/update", params = {"id"})
    private User updateUser(@RequestParam("id") Long id, @RequestBody User user){
        return userService.updateUser(id, user);
    }

    @DeleteMapping(path = "/delete", params = {"id"})
    private void removeUser(@RequestParam("id") Long id){
        userService.removeUser(id);
    }

}
