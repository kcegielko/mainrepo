package pl.cegielko.app.security.utility;

import lombok.AllArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import pl.cegielko.app.security.models.User;
import pl.cegielko.app.security.services.UserService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@AllArgsConstructor
@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    private final UserService userService;
    private final Jwt jwt;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

    final String authorizationHeader = httpServletRequest.getHeader("Authorization");

    String username = null;

    String jwtToken = null;

    if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")){

        jwtToken = authorizationHeader.substring(7);
        username = jwt.extractUsername(jwtToken);

    }

    if (username != null && SecurityContextHolder.getContext().getAuthentication() == null){

        User user = (User) this.userService.loadUserByUsername(username);

        if (jwt.validateToken(jwtToken, user)){

            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
            usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));

            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

        }

    }

    filterChain.doFilter(httpServletRequest, httpServletResponse);

    }
}
